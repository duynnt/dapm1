<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
session_start();
require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');


/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
$router->add('/dapm1/public/', ['controller' => 'Home', 'action' => 'index']);
// $router->add('/dapm1/public/demopage', ['controller' => 'About', 'action' => 'index']);
// $router->add('/dapm1/public/', ['controller' => 'Home', 'action' => 'getAll']);
$router->add('/dapm1/public/dashboard', ['controller' => 'Admin', 'action' => 'index']);
// login
$router->add('/dapm1/public/login', ['controller' => 'Login', 'action' => 'login']);
$router->add('/dapm1/public/register', ['controller' => 'Login', 'action' => 'register']);
$router->add('/dapm1/public/registeraccount', ['controller' => 'Login', 'action' => 'registeracount']);
$router->add('/dapm1/public/loginaccount', ['controller' => 'Login', 'action' => 'checklogin']);
$router->add('/dapm1/public/datlamphanmem', ['controller' => 'PhanMem', 'action' => 'phanmem']);
$router->add('/dapm1/public/updateinformation', ['controller' => 'Information', 'action' => 'index']);
$router->add('/dapm1/public/buysoftware', ['controller' => 'PhanMem', 'action' => 'buysoftware']);
// $router->add('/dapm1/public/checkout', ['controller' => 'PhanMem', 'action' => 'products']);
// $router->add('/dapm1/public/addtocart', ['controller' => 'PhanMem', 'action' => 'addtocart']);
$router->add('/dapm1/public/ordersoftware', ['controller' => 'PhanMem', 'action' => 'softwareorder']);
$router->add('/dapm1/public/controlsoftware', ['controller' => 'PhanMem', 'action' => 'controlsoftware']);
$router->add('/dapm1/public/controlsoftware-asigment', ['controller' => 'PhanMem', 'action' => 'controlsoftwareassigment']);
$router->add('/dapm1/public/browse-request', ['controller' => 'PhanMem', 'action' => 'browserequest']);
$router->add('/dapm1/public/updateSoftware', ['controller' => 'PhanMem', 'action' => 'updateSoftware']);
//$router->add('/dapm1/public/updateCategory', ['controller' => 'Category', 'action' => 'updateCategory']);
$router->add('/dapm1/public/admin/home', ['controller' => 'Admin', 'action' => 'adminmanagerment']);
$router->add('/dapm1/public/admin/updateProfile', ['controller' => 'Admin', 'action' => 'updateProfile']);
$router->add('/dapm1/public/duyet-yeu-cau-don-dat-hang', ['controller' => 'Admin', 'action' => 'home']);
$router->add('/dapm1/public/cap-nhat-du-an', ['controller' => 'Admin', 'action' => 'updateproject']);
$router->add('/dapm1/public/khach-hang', ['controller' => 'Admin', 'action' => 'profile']);

// nv pm
$router->add('/dapm1/public/cap-nhat-du-an-v1', ['controller' => 'Admin', 'action' => 'browrequets']);
$router->add('/dapm1/public/duyet-don-dat-hang', ['controller' => 'Admin', 'action' => 'assetorder']);
$router->add('/dapm1/public/cap-nhat-thong-tin', ['controller' => 'Admin', 'action' => 'updateinfo']);
// hợp đồng dự án
$router->add('/dapm1/public/cap-nhat-hop-dong-du-an', ['controller' => 'Admin', 'action' => 'updatecontact']);
// cập nhật phần mền
$router->add('/dapm1/public/cap-nhat-phan-mem', ['controller' => 'Admin', 'action' => 'updatesofeware']);
// cập nhật danh mục
$router->add('/dapm1/public/cap-nhat-danh-muc', ['controller' => 'Admin', 'action' => 'updatecategory']);
//cập nhật thông tin nv marketing
$router->add('/dapm1/public/cap-nhat-thong-tin-mt', ['controller' => 'Admin', 'action' => 'updateinfomt']);
// logout
$router->add('/dapm1/public/logout', ['controller' => 'Admin', 'action' => 'logout']);
// đặt làm pm
$router->add('/dapm1/public/dat-lam-phan-mem', ['controller' => 'PhanMem', 'action' => 'contentsingle']);
// chi tiết duyệt yêu cầu /dapm1/public/dat-lam-phan-mem/chi-tiet/{idrequest:\d+}
$router->add('/dapm1/public/dat-lam-phan-mem/chi-tiet/{idrequest:\d+}', ['controller' => 'SoftWare', 'action' => 'detailordersoftware']);
$router->add('/dapm1/public/dat-lam-phan-mem/chi-tiet/accept', ['controller' => 'SoftWare', 'action' => 'acceptordersoftware']);
$router->add('/dapm1/public/cap-nhat-du-an-v1/chi-tiet', ['controller' => 'SoftWare', 'action' => 'updatePM']);
//nv marketing
$router->add('/dapm1/public/cap-nhat-du-an/chinh-sua/{idrequest:\d+}', ['controller' => 'SoftWare', 'action' => 'editproject']);
$router->add('/dapm1/public/cap-nhat-du-an/them-moi', ['controller' => 'SoftWare', 'action' => 'addnewproject']);
$router->add('/dapm1/public/duyet-yeu-cau-don-dat-hang/chi-tiet/{idrequest:\d+}', ['controller' => 'SoftWare', 'action' => 'acceptmketing']);
$router->add('/dapm1/public/duyet-yeu-cau-don-dat-hang/chi-tiet/accept', ['controller' => 'SoftWare', 'action' => 'acceptordermketing']);
// dự án
$router->add('/dapm1/public/insertproject', ['controller' => 'SoftWare', 'action' => 'insertsoftware']);
$router->add('/dapm1/public/updateproject', ['controller' => 'SoftWare', 'action' => 'updatesoftwarecontrol']);
$router->add('/dapm1/public/removerproject', ['controller' => 'SoftWare', 'action' => 'removerproject']);
$router->add('/dapm1/public/finishproject', ['controller' => 'SoftWare', 'action' => 'finishproject']);
$router->add('/dapm1/public/sendrequestorder', ['controller' => 'SoftWare', 'action' => 'sendrqorder']);
$router->add('/dapm1/public/getdescriptions', ['controller' => 'SoftWare', 'action' => 'getdescriptions']);
$router->dispatch($_SERVER['REQUEST_URI']);
