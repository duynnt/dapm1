<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require dirname(__DIR__) . '/Libary/headerlib.php';
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
</head>
<style>
    .sofeware{
        padding: 30px 0;
    }
    .sofeware input:focus{
        outline: none;
    }
    .sofeware .table-bordered{
        border: 1px solid #ddd;
    }
    table {
        padding: 15px 0;
    }
    table.dataTable thead th, table.dataTable thead td{
        border: 1px solid #e7e7e7;
    }
    table.dataTable.no-footer{
        border-bottom: 1px solid #e7e7e7;
    }
    .addnewproject{
        float: left;
        padding-bottom: 30px;
    }
    .sofeware .action{
        text-align: center;
    }
    
</style>
<body>

    <div class="wrapper fixed__footer">
        <?php
        require dirname(__DIR__) . '/Block/headerislogin.php';
        ?>
        <div class="grow">
            <div class="container">
                <h2>Danh Sách Danh Mục</h2>
            </div>
        </div>
        <!-- grow -->
        <div class="sofeware">
            <div class="container">
                <div class="addnewproject">
                    <button type="button" class="btn btn-primary">Thêm mới</button>
                </div>
                <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên danh mục</th>
                            <th>Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Dự án có sẵn</td>
                            <td class="action">
                                <button type="button" class="btn btn-primary">Sửa</button>
                                <button type="button" class="btn btn-success">Xóa</button>
                            </td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Dự án đặt làm</td>
                            <td class="action">
                                <button type="button" class="btn btn-primary">Sửa</button>
                                <button type="button" class="btn btn-success">Xóa</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="clearfix"> </div>
            </div>
        </div>
        <?php
        require dirname(__DIR__) . '/Block/footer.php';
        ?>
    </div>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script>
     
        $(document).ready(function() {
            $('body,html').animate({scrollTop: 556}, 800); 
            $('#table_id').DataTable({
                "lengthMenu": [10, 50, 100, 500, 1000, 5000],
                "language": {
                    "sLengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
                    "sZeroRecords": "Không tìm thấy dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "sInfoEmpty": "Không có dữ liệu nào",
                    "sInfoFiltered": "(được lọc từ tổng sô _MAX_ trong dữ liệu)",
                    "sSearch": "Tìm kiếm:",
                    "oPaginate": {
                        "sNext": "Sau",
                        "sPrevious": "Trước"
                    },
                }
            });
        });
    </script>

</body>

</html>