<div class="text-center footer">
    <div class="container">
        <div class="row">
            <p class="footernote">
                Liên hệ với chúng tôi
            </p>
            <ul class="social-iconsfooter">
                <li><a href="#"><i class="fa fa-phone"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="newui/js/jquery-.js"></script>
<script src="newui/js/bootstrap.min.js"></script>
<script src="newui/js/anim.js"></script>
<script src="newui/js/validate.js"></script>
<script>
    function Notification(title, msg, $false) {
	    /*change*/
	    var $content = "<div class='dialog-ovelay'>" +
	      "<div class='dialog'><header>" +
	      " <h3> " + title + " </h3> " +
	      "<i class='fa fa-close'></i>" +
	      "</header>" +
	      "<div class='dialog-msg'>" +
	      " <p> " + msg + " </p> " +
	      "</div>" +
	      "<footer>" +
	      "<div class='controls'>" +
	      " <button class='button button-default cancelAction'>" + $false + "</button> " +
	      "</div>" +
	      "</footer>" +
	      "</div>" +
	      "</div>";
	    $('body').prepend($content);
	    $('.cancelAction, .fa-close').click(function() {
	      $(this).parents('.dialog-ovelay').fadeOut(500, function() {
	        $(this).remove();
	      });
	    });

	  }

	  function Confirm(title, msg, $true, $false, $link) {
	    /*change*/
	    var $content = "<div class='dialog-ovelay'>" +
	      "<div class='dialog'><header>" +
	      " <h3> " + title + " </h3> " +
	      "<i class='fa fa-close'></i>" +
	      "</header>" +
	      "<div class='dialog-msg'>" +
	      " <p> " + msg + " </p> " +
	      "</div>" +
	      "<footer>" +
	      "<div class='controls'>" +
	      " <button class='button button-danger doAction'>" + $true + "</button> " +
	      " <button class='button button-default cancelAction'>" + $false + "</button> " +
	      "</div>" +
	      "</footer>" +
	      "</div>" +
	      "</div>";
	    $('body').prepend($content);
	    $('.doAction').click(function() {
	      window.open($link, "_blank"); /*new*/
	      $(this).parents('.dialog-ovelay').fadeOut(500, function() {
	        $(this).remove();
	      });
	    });
	    $('.cancelAction, .fa-close').click(function() {
	      $(this).parents('.dialog-ovelay').fadeOut(500, function() {
	        $(this).remove();
	      });
	    });

	  }
</script>