<header class="item header margin-top-0">
<div class="wrapper">
	<nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
			<i class="fa fa-bars"></i>
			<span class="sr-only">Slider bar</span>
			</button>
			<a href="/dapm1/public/" class="navbar-brand brand"> SofeWareNT </a>
		</div>
		<div id="navbar-collapse-02" class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="propClone"><a href="/dapm1/public/">Trang chủ</a></li>
                <li class="propClone"><a href="/dapm1/public/login">Đăng nhập</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="text-center col-md-12">
				<div class="text-pageheader">
					<div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.0s">
						  Dự Án Phần Mềm Mới Nhất
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</header>