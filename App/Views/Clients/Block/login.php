	<style>
		.account {
			text-align: center;
		}

		.account-top span {
			padding-right: 20px;
		}

		.account-top input {
			width: 250px;
		}

		.account-top input:focus {
			outline: none;

		}

		#submitlogin {
			width: auto;
			margin-top: 30px;
		}

		.account-top form {
			padding: 30px 0 100px 0;
		}

		.account-top form div {
			margin-bottom: 20px;
		}

		/* confirm dialog*/
		body {
			font-family: sans-serif
		}

		.dialog-ovelay {
			background-color: rgba(0, 0, 0, 0.836);
			position: fixed;
			width: 100%;
			height: 100%;
			z-index: 2000;
			top: 0;
			left: 0;
		}

		.dialog-ovelay .dialog {
			width: 400px;
			margin: 100px auto 0;
			background-color: #fff;
			box-shadow: 0 0 20px rgba(0, 0, 0, .2);
			border-radius: 3px;
			overflow: hidden
		}

		.dialog-ovelay .dialog header {
			padding: 10px 8px;
			background-color: #f6f7f9;
			border-bottom: 1px solid #e5e5e5
		}

		.dialog-ovelay .dialog header h3 {
			font-size: 14px;
			margin: 0;
			color: #555;
			display: inline-block
		}

		.dialog-ovelay .dialog header .fa-close {
			float: right;
			color: #c4c5c7;
			cursor: pointer;
			transition: all .5s ease;
			padding: 0 2px;
			border-radius: 1px
		}

		.dialog-ovelay .dialog header .fa-close:hover {
			color: #b9b9b9
		}

		.dialog-ovelay .dialog header .fa-close:active {
			box-shadow: 0 0 5px #673AB7;
			color: #a2a2a2
		}

		.dialog-ovelay .dialog .dialog-msg {
			padding: 12px 10px
		}

		.dialog-ovelay .dialog .dialog-msg p {
			margin: 0;
			font-size: 15px;
			color: #333
		}

		.dialog-ovelay .dialog footer {
			border-top: 1px solid #e5e5e5;
			padding: 8px 10px
		}

		.dialog-ovelay .dialog footer .controls {
			direction: rtl
		}

		.dialog-ovelay .dialog footer .controls .button {
			padding: 5px 15px;
			border-radius: 3px
		}

		.button {
			cursor: pointer
		}

		.button-default {
			background-color: rgb(248, 248, 248);
			border: 1px solid rgba(204, 204, 204, 0.5);
			color: #5D5D5D;
		}

		.button-danger {
			background-color: #f44336;
			border: 1px solid #d32f2f;
			color: #f5f5f5
		}

		.link {
			padding: 5px 10px;
			cursor: pointer
		}
	</style>
	<!-- grow -->
	<div class="grow">
		<div class="container text-center">
			<h2>Đăng nhập</h2>
		</div>
	</div>
	<!-- grow -->
	<!--content-->
	<div class="container">
		<div class="account">
			<div class="account-pass">
				<div class="account-top">
					<form>
						<div>
							<span>Tài khoản</span>
							<input id="username" type="text" required>
						</div>
						<div>
							<span>Mật khẩu</span>
							<input id="password" type="password" required>
						</div>
						<input id="submitlogin" type="submit" value="Đăng nhập">
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>

	</div>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
		$('form').submit(function(evt) {
			evt.preventDefault();
		});
		$('#submitlogin').click(function() {
			let username = $('#username').val();
			let password = $('#password').val();
			$.post("/dapm1/public/loginaccount", {
				username: username,
				password: password,
			}, function(responsive) {
				if (responsive == 3) {
					window.location.href = "/dapm1/public/";
				} else {
					if (responsive == 0 || responsive == 1 || responsive == 2) {
						window.location.href = "/dapm1/public/admin/home";
					} else {
						Notification('Thông báo', 'Tài khoản hoặc mật khẩu không đúng !!', 'Quay lại', ); /*change*/
					}
				}
			});
		});

		function Notification(title, msg, $false) {
			/*change*/
			var $content = "<div class='dialog-ovelay'>" +
				"<div class='dialog'><header>" +
				" <h3> " + title + " </h3> " +
				"<i class='fa fa-close'></i>" +
				"</header>" +
				"<div class='dialog-msg'>" +
				" <p> " + msg + " </p> " +
				"</div>" +
				"<footer>" +
				"<div class='controls'>" +
				" <button class='button button-default cancelAction'>" + $false + "</button> " +
				"</div>" +
				"</footer>" +
				"</div>" +
				"</div>";
			$('body').prepend($content);
			$('.cancelAction, .fa-close').click(function() {
				$(this).parents('.dialog-ovelay').fadeOut(500, function() {
					$(this).remove();
				});
			});

		}

		function Confirm(title, msg, $true, $false, $link) {
			/*change*/
			var $content = "<div class='dialog-ovelay'>" +
				"<div class='dialog'><header>" +
				" <h3> " + title + " </h3> " +
				"<i class='fa fa-close'></i>" +
				"</header>" +
				"<div class='dialog-msg'>" +
				" <p> " + msg + " </p> " +
				"</div>" +
				"<footer>" +
				"<div class='controls'>" +
				" <button class='button button-danger doAction'>" + $true + "</button> " +
				" <button class='button button-default cancelAction'>" + $false + "</button> " +
				"</div>" +
				"</footer>" +
				"</div>" +
				"</div>";
			$('body').prepend($content);
			$('.doAction').click(function() {
				window.open($link, "_blank"); /*new*/
				$(this).parents('.dialog-ovelay').fadeOut(500, function() {
					$(this).remove();
				});
			});
			$('.cancelAction, .fa-close').click(function() {
				$(this).parents('.dialog-ovelay').fadeOut(500, function() {
					$(this).remove();
				});
			});

		}
	</script>