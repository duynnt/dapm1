<div class="footer w3layouts">
  <div class="container">
    <div class="footer-top-at w3">

      <div class="col-md-3 amet-sed w3l">
        <h4>Thông Tin</h4>
        <ul class="nav-bottom">
          <li><a href="#">Làm sao để mua hàng</a></li>
          <li><a href="#">Hỏi và Đáp</a></li>
          <li><a href="contact.html">Địa chỉ</a></li>
          <li><a href="#">Giao hàng</a></li>
          <li><a href="#">Thành viên</a></li>
        </ul>
      </div>
      <div class="col-md-3 amet-sed w3ls">
        <h4>Danh Mục</h4>
        <ul class="nav-bottom">
          <li><a href="#">Nhà Ở</a></li>
          <li><a href="#">Bất Động Sản</a></li>
          <li><a href="#">Học Tập</a></li>
          <li><a href="#">Nghệ Thuật</a></li>
          <li><a href="#">Khoa Học</a></li>
        </ul>

      </div>
      <div class="col-md-3 amet-sed agileits">
        <h4>Thông tin liên hệ</h4>
        <div class="stay agileinfo">
          <div class="stay-left wthree">
            <form action="#" method="post">
              <input type="text" placeholder="Nhập email ... " required="">
            </form>
          </div>
          <div class="btn-1 w3-agileits">
            <form action="#" method="post">
              <input type="submit" value="Gửi">
            </form>
          </div>
          <div class="clearfix"> </div>
        </div>

      </div>
      <div class="col-md-3 amet-sed agileits-w3layouts">
        <h4>Liên Hệ</h4>
        <p>Nguyễn Hữu Trạch</p>
        <p>Số điện thoại : +84 34 995 0792</p>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
  <div class="footer-class w3-agile">
    <p>© 2021 Nhom_6 . All Rights Reserved | Design by nnt</a> </p>
  </div>
</div>
<script>
	
  function Notification(title, msg, $false) {
	    /*change*/
	    var $content = "<div class='dialog-ovelay'>" +
	      "<div class='dialog'><header>" +
	      " <h3> " + title + " </h3> " +
	      "<i class='fa fa-close'></i>" +
	      "</header>" +
	      "<div class='dialog-msg'>" +
	      " <p> " + msg + " </p> " +
	      "</div>" +
	      "<footer>" +
	      "<div class='controls'>" +
	      " <button class='button button-default cancelAction'>" + $false + "</button> " +
	      "</div>" +
	      "</footer>" +
	      "</div>" +
	      "</div>";
	    $('body').prepend($content);
	    $('.cancelAction, .fa-close').click(function() {
	      $(this).parents('.dialog-ovelay').fadeOut(500, function() {
	        $(this).remove();
	      });
	    });

	  }

	  function Confirm(title, msg, $true, $false, $link) {
	    /*change*/
	    var $content = "<div class='dialog-ovelay'>" +
	      "<div class='dialog'><header>" +
	      " <h3> " + title + " </h3> " +
	      "<i class='fa fa-close'></i>" +
	      "</header>" +
	      "<div class='dialog-msg'>" +
	      " <p> " + msg + " </p> " +
	      "</div>" +
	      "<footer>" +
	      "<div class='controls'>" +
	      " <button class='button button-danger doAction'>" + $true + "</button> " +
	      " <button class='button button-default cancelAction'>" + $false + "</button> " +
	      "</div>" +
	      "</footer>" +
	      "</div>" +
	      "</div>";
	    $('body').prepend($content);
	    $('.doAction').click(function() {
	      window.open($link, "_blank"); /*new*/
	      $(this).parents('.dialog-ovelay').fadeOut(500, function() {
	        $(this).remove();
	      });
	    });
	    $('.cancelAction, .fa-close').click(function() {
	      $(this).parents('.dialog-ovelay').fadeOut(500, function() {
	        $(this).remove();
	      });
	    });

	  }
</script>