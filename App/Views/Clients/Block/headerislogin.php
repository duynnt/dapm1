<div class="header">
	<div class="header-top">
		<div class="container">
			<div class="social">
				<ul>
					<li><a href="#"><i class="facebok"> </i></a></li>
					<li><a href="#"><i class="twiter"> </i></a></li>
					<li><a href="#"><i class="inst"> </i></a></li>
					<li><a href="#"><i class="goog"> </i></a></li>
					<div class="clearfix"></div>
				</ul>
			</div>
			<div class="header-left">

				<div class="search-box">
					<div id="sb-search" class="sb-search">
						<form action="#" method="post">
							<input class="sb-search-input" placeholder="Enter your search term..." type="search" id="search">
							<input class="sb-search-submit" type="submit" value="">
							<span class="sb-icon-search"> </span>
						</form>
					</div>
				</div>

				<!-- search-scripts -->
				<script src="js/classie.js"></script>
				<script src="js/uisearch.js"></script>
				<script>
					new UISearch(document.getElementById('sb-search'));
				</script>
				<!-- //search-scripts -->

				<div class="ca-r">
					<div class="cart box_1">
						<a href="checkout.html">
							<h3>
								<div class="total">
									<span class="simpleCart_total"></span>
								</div>
								<img src="images/cart.png" alt="" />
							</h3>
						</a>
						<p><a href="javascript:;" class="simpleCart_empty"></a></p>

					</div>
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>
	</div>
	<div class="container">
		<div class="head-top">
			<div class="logo">
				<h1><a href="index.html">CusHus</a></h1>
			</div>
			<div class=" h_menu4">
				<ul class="memenu skyblue">
					<li><a class="color8" href="/dapm1/public/">Trang Chủ</a></li>
					<li><a class="color1" href="#">Giới Thiệu</a>
					</li>
					<li class="grid"><a class="color2" href="#">Liên Hệ</a>
					</li>
					<li><a class="color4" href="">Xin Chào: <?php echo $_SESSION['login']['name'] ?></a>
						<div class="mepanel">
							<div class="col1">
								<div class="h_nav">
									<ul>
										<li><a style="text-decoration: none; font-size: 15px;" href="/dapm1/public/logout">Đăng xuất</a></li>
									</ul>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<div class="banner">
	<div class="container">
		<script src="js/responsiveslides.min.js"></script>
		<script>
			$(function() {
				$("#slider").responsiveSlides({
					auto: true,
					nav: true,
					speed: 500,
					namespace: "callbacks",
					pager: true,
				});
			});
		</script>
		<div id="top" class="callbacks_container">
			<ul class="rslides" id="slider">
				<li>

					<div class="banner-text">
						<h3>Chào Mừng Bạn Đến Với Website</h3>
						<p>Nội dung thân thiện, cuốn hút. Bạn nên viết ngắn gọn, nêu bật lợi ích thiết thực nhất cho người dùng. Bạn nên sử dụng văn phong tự nhiên hơn</p>

					</div>

				</li>
				<li>

					<div class="banner-text">
						<h3>Chào Mừng Bạn Đến Với Website</h3>
						<p>Nội dung thân thiện, cuốn hút. Bạn nên viết ngắn gọn, nêu bật lợi ích thiết thực nhất cho người dùng. Bạn nên sử dụng văn phong tự nhiên hơn</p>

					</div>

				</li>
				<li>
					<div class="banner-text">
						<h3>Chào Mừng Bạn Đến Với Website</h3>
						<p>Nội dung thân thiện, cuốn hút. Bạn nên viết ngắn gọn, nêu bật lợi ích thiết thực nhất cho người dùng. Bạn nên sử dụng văn phong tự nhiên hơn</p>

					</div>

				</li>
			</ul>
		</div>

	</div>
</div>