<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require dirname(__DIR__) . '/Libary/headerlib.php';
  ?>
</head>
<style>
.submit {
    margin-left: 500px;
	margin-top:1%;
    background: white;
    color: green;
    border: 2px solid green;
    padding: 10px 25px;
    outline: none;
    border-radius: 4px;
    text-transform: uppercase;
    font-weight: bold;
    animation: .3s ease;
    cursor: pointer;
}

input {
    font-family: UTM Neo Sans Intel Regular;
}

.imgcart {
    display: block;
    width: 40px;
    height: 250px;
}
.cart-item{
	width: 100%;
	font-size: 22px;
	font-family: UTM Neo Sans Intel Regular;
}
	</style>
<body>

  <div class="wrapper fixed__footer">
    <?php
    require dirname(__DIR__) . '/Block/headerislogin.php';
    ?>
 	<!-- grow -->
	 <div class="grow">
		<div class="container">
			<h2>Giỏ Hàng</h2>
		</div>
	</div>
	<!-- grow -->
	<div class="container">
		<div class="check">
			<h1 style="font-size: 22px;font-family: UTM Neo Sans Intel Regular;">Giỏ hàng của bạn (1)</h1>
			<div class="col-md-9 cart-items">

				<script>$(document).ready(function (c) {
						$('.close1').on('click', function (c) {
							$('.cart-header').fadeOut('slow', function (c) {
								$('.cart-header').remove();
							});
						});
					});
				</script>
				<div class="cart-header">
					<div class="close1"> </div>
					<div class="cart-sec simpleCart_shelfItem">
						<div class="cart-item cyc">
							<img src="images/pic1.jpg" class="imgcart" alt="" />
						</div>
						<div class="cart-item">
							<p>Tên Sản Phẩm: Quản Lý Khách Sạn</p>
							<p>Mã SP: 3578</p>
							<p>Giá Tiền: $500.000</p>
							<p>Giảm Giá: ----</p>
							<p>Tổng Tiền: $500.000</p>			
						</div>
						<div class="clearfix"></div>

					</div>
					<div>
						<input type="submit" value="Thanh Toán" class="submit" name="order">
					</div>
				</div>
				</div>
			</div>
		
		</div>
	</div>
	
    <?php
    require dirname(__DIR__) . '/Block/footer.php';
    ?>
  </div>
</body>

</html>