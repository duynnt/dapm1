<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require dirname(__DIR__) . '/Libary/headerlib.php';
  ?>
</head>
<style>
.submit {
    margin-left: 500px;
	margin-top:1%;
    background: white;
    color: green;
    border: 2px solid green;
    padding: 10px 25px;
    outline: none;
    border-radius: 4px;
    text-transform: uppercase;
    font-weight: bold;
    animation: .3s ease;
    cursor: pointer;
}

input {
    font-family: UTM Neo Sans Intel Regular;
}
	</style>
<body>

  <div class="wrapper fixed__footer">
    <?php
    require dirname(__DIR__) . '/Block/headerislogin.php';
    ?>
 <div class="grow">
		<div class="container">
			<h2>Chi Tiết Sản Phẩm</h2>
		</div>
	</div>
	<!-- grow -->
	<div class="product">
		<div class="container">

			<div class="product-price1">
				<div class="top-sing">
					<div class="col-md-7 single-top">
						<div class="flexslider">
							<ul class="slides">
								<li data-thumb="images/si.jpg">
									<div class="thumb-image"> <img src="images/si.jpg" data-imagezoom="true"
											class="img-responsive"> </div>
								</li>
								<li data-thumb="images/si1.jpg">
									<div class="thumb-image"> <img src="images/si1.jpg" data-imagezoom="true"
											class="img-responsive"> </div>
								</li>
								<li data-thumb="images/si2.jpg">
									<div class="thumb-image"> <img src="images/si2.jpg" data-imagezoom="true"
											class="img-responsive"> </div>
								</li>
								<li data-thumb="images/si3.jpg">
									<div class="thumb-image"> <img src="images/si3.jpg" data-imagezoom="true"
											class="img-responsive"> </div>
								</li>
							</ul>
						</div>

						<div class="clearfix"> </div>
						<!-- slide -->


						<!-- FlexSlider -->
						<script defer src="js/jquery.flexslider.js"></script>
						<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

						<script>
							// Can also be used with $(document).ready()
							$(window).load(function () {
								$('.flexslider').flexslider({
									animation: "slide",
									controlNav: "thumbnails"
								});
							});
						</script>

					</div>
					<div class="col-md-5 single-top-in simpleCart_shelfItem">
						<div class="single-para ">
						<h4 style="font-family: UTM Neo Sans Intel Regular;">Tên Sản Phẩm:</h4>
							<h5>QUẢN LÝ KHÁCH SẠN</h5>
							<!-- <div class="star-on">

								<div class="review">
									<a href="#"> 1 đánh giá của khách hàng </a>
								</div>
								<div class="clearfix"> </div>
							</div> -->
							<h4 style="font-family: UTM Neo Sans Intel Regular;">Giá Sản Phẩm:</h4>
							<h5 class="item_price">$ 500.00</h5>
							<p style="font-size: 22px;font-family: UTM Neo Sans Intel Regular;">Quản lý chuỗi khách sạn kiểm soát tất cả thông tin chỉ trong một click.
							Module quản lý phòng thể hiện số phòng đang quản lý, tình trạng phòng. 
							Các thông tin về phòng như phòng đơn, phòng đôi, giá tiền phòng…
						 </p>
							<!-- <div class="available">
								<ul>
									<li>Color
										<select>
											<option>Silver</option>
											<option>Black</option>
											<option>Dark Black</option>
											<option>Red</option>
										</select>
									</li>
									<li class="size-in">Size<select>
											<option>Large</option>
											<option>Medium</option>
											<option>small</option>
											<option>Large</option>
											<option>small</option>
										</select></li>
									<div class="clearfix"> </div>
								</ul>
							</div> -->

							<!-- <a href="#" class="add-cart item_add">ADD TO CART</a> -->
							<!-- <a href="/dapm1/public/addtocart">Thêm Vào Giỏ Hàng</a> -->

						</div>
					</div>
					<div class="clearfix"> </div>
					<a href="/dapm1/public/addtocart"><input type="submit" value="Thêm vào giỏ hàng" class="submit" name="order"></a>
				</div>
				
				<!---->
				<div class="container">
					<h2>Một Số Sản Phẩm Khác</h2><br/><br/>
				</div>

				<div class=" bottom-product">
					<div class="col-md-4 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi3.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua Ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>QUẢN LÝ NHÀ Ở</span><br>Sản phẩm hot/p>
						<div class="ca-rt">
							<a href="/dapm1/public/checkout" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
					</div>
					<div class="col-md-4 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi1.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua Ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>HỆ THỐNG CAFE</span><br>Sản phẩm hot</p>
						<div class="ca-rt">
							<a href="/dapm1/public/checkout" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
					</div>
					<div class="col-md-4 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi4.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua Ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>QUẢN LÝ BẾN XE</span><br>Sản phẩm hot</p>
						<div class="ca-rt">
							<a href="/dapm1/public/checkout" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>

			<div class="clearfix"> </div>
		</div>
	</div>
    <?php
    require dirname(__DIR__) . '/Block/footer.php';
    ?>
  </div>
</body>

</html>