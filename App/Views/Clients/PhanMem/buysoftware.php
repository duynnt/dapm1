<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require dirname(__DIR__) . '/Libary/headerlib.php';
  ?>
</head>
<style>

</style>
<body>

  <div class="wrapper fixed__footer">
    <?php
    require dirname(__DIR__) . '/Block/headerislogin.php';
    ?>
   <div class="grow">
		<div class="container">
			<h2>Sản Phẩm</h2>
		</div>
	</div>
	<!-- grow -->
	<div class="pro-du">
		<div class="container">
			<div class="col-md-9 product1">
				<div class=" bottom-product">
					<div class="col-md-6 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi3.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>QUẢN LÝ KHÁCH SẠN</span><br>Sản phẩm hot</p>
						<div class="ca-rt">
							<a href="#" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-6 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi1.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>QUẢN LÝ NHÀ Ở</span><br>Sản phẩm hot</p>
						<div class="ca-rt">
							<a href="#" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class=" bottom-product">
					<div class="col-md-6 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi5.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>HỆ THỐNG CAFE</span><br>Sản phẩm hot</p>
						<div class="ca-rt">
							<a href="#" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-6 bottom-cd simpleCart_shelfItem">
						<div class="product-at ">
							<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/pi.jpg" alt="">
								<div class="pro-grid">
									<span class="buy-in">Mua ngay</span>
								</div>
							</a>
						</div>
						<p class="tun"><span>QUẢN LÝ BẾN XE</span><br>Sản phẩm hot</p>
						<div class="ca-rt">
							<a href="#" class="item_add">
								<p class="number item_price"><i> </i>$500.00</p>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"> </div>
				</div>


			</div>
			<div class="col-md-3 prod-rgt">
				<div class=" pro-tp">
					<div class="pl-lft">
						<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/l2.jpg" alt=""></a>
					</div>
					<div class="pl-rgt">
						<h6><a href="/dapm1/public/checkout">TRIBECA LIVING</a></h6>
						<p><a href="/dapm1/public/checkout">450$</a></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class=" pro-tp">
					<div class="pl-lft">
						<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/l3.jpg" alt=""></a>
					</div>
					<div class="pl-rgt">
						<h6><a href="/dapm1/public/checkout">TRIBECA LIVING</a></h6>
						<p><a href="/dapm1/public/checkout">450$</a></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class=" pro-tp">
					<div class="pl-lft">
						<a href="/dapm1/public/checkout"><img class="img-responsive" src="images/l1.jpg" alt=""></a>
					</div>
					<div class="pl-rgt">
						<h6><a href="/dapm1/public/checkout">TRIBECA LIVING</a></h6>
						<p><a href="/dapm1/public/checkout">450$</a></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<!-- <div class="pr-btm">
					<h4>What Our Client Say</h4>
					<img class="img-responsive" src="images/pi.jpg" alt="">
					<h6>John</h6>
					<p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry's
						standard dummy text ever since the 1500s,</p>
				</div> -->
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
    <?php
    require dirname(__DIR__) . '/Block/footer.php';
    ?>
  </div>
</body>

</html>