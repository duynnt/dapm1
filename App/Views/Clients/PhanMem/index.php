<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require dirname(__DIR__) . '/Libary/headerlib.php';
  ?>
</head>

<body>

  <div class="wrapper fixed__footer">
    <?php
    require dirname(__DIR__) . '/Block/headerislogin.php';
    ?>
    <!-- grow -->
	<div class="grow">
		<div class="container">
			<h2>Giỏ Hàng</h2>
		</div>
	</div>
	<!-- grow -->
	<div class="container">
		<div class="check">
			<h1>My Shopping Bag (1)</h1>
			<div class="col-md-12 cart-items">

				<script>$(document).ready(function (c) {
						$('.close1').on('click', function (c) {
							$('.cart-header').fadeOut('slow', function (c) {
								$('.cart-header').remove();
							});
						});
					});
				</script>
				<div class="cart-header">
					<div class="close1"> </div>
					<div class="cart-sec simpleCart_shelfItem">
						<div class="cart-item cyc">
							<img src="images/pic1.jpg" class="img-responsive" alt="" />
						</div>
						<div class="cart-item-info">
							<h3><a href="#">Mountain Hopper(XS R034)</a><span>Model No: 3578</span></h3>
							<ul class="qty">
								<li>
									<p>Số Lượng : 1</p>
								</li>
								<li>
									<p>Giá Tiền: $500.000</p>
								</li>
							</ul>

							<div class="delivery">
								<p>Hoàn Thành Sau 2 Tháng</p>
								<div class="clearfix"></div>
							</div>
							<div>
								<ul>
									<li>
										<p>Giảm Giá: ---</p>
									</li>
									<li>
										<p>Tổng Tiền: $500.000</p>
									</li>
								</ul>
							</div>
							<div>
								<input type="submit" value="Đặt Mua">
							</div>
						</div>
						<div class="clearfix"></div>

					</div>
				</div>
				</div>
			</div>
			<!-- <div class="col-md-3 cart-total">
				<a class="continue" href="#">Continue to basket</a>
				<div class="price-details">
					<h3>Price Details</h3>
					<span>Total</span>
					<span class="total1">6200.00</span>
					<span>Discount</span>
					<span class="total1">---</span>
					<span>Delivery Charges</span>
					<span class="total1">150.00</span>
					<div class="clearfix"></div>
				</div>
				<ul class="total_price">
					<li class="last_price">
						<h4>TOTAL</h4>
					</li>
					<li class="last_price"><span>6350.00</span></li>
					<div class="clearfix"> </div>
				</ul>


				<div class="clearfix"></div>
				<a class="order" href="#">Place Order</a>
				<div class="total-item">
					<h3>OPTIONS</h3>
					<h4>COUPONS</h4>
					<a class="cpns" href="#">Apply Coupons</a>
					<p><a href="#">Log In</a> to use accounts - linked coupons</p>
				</div>
			</div>

			<div class="clearfix"> </div> -->
		</div>
	</div>
    <?php
    require dirname(__DIR__) . '/Block/footer.php';
    ?>
  </div>
</body>

</html>