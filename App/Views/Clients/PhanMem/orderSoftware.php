<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require dirname(__DIR__) . '/Libary/headerlib.php';
  ?>
<style>
    .software {
        padding-bottom: 20px;
        padding-top: 30px;
    }
    .requriedment{
        width: 70%;
        margin: auto;
   
    }
    .account{
        padding: 0;
    }
    .requriedment h5 {
        font-size: 1.7em;
        padding: 8px 0;
        font-weight: 600;
        text-align: center;
    }
    #submitlogin{
        float: right;
    }
    .account-top #descriptionsoftware{
        height: 150px;
    }
    #submitlogin{
        padding: 10px 15px;
        width: 100%;    
    }
    .requriedment span{
        font-size: 20px;
    }
</style>
</head>

<body>

  <div class="wrapper fixed__footer">
    <?php
    require dirname(__DIR__) . '/Block/headerislogin.php';
    ?>
    <div class="container">
      <div class="software row">
        <div style=" width: 60%; margin:auto;">
        <div class="account requriedment">
            <div class="title">
                <h5>Yêu cầu đặt làm dự án phần mềm</h5>
            </div>
	        <div class="account-pass">
	            <div class="account-top">
	                <form>
	                    <div>
	                        <span>Mô tả</span>
	                        <textarea id="descriptionsoftware" type="text" required></textarea>
	                    </div>
	                    <div>
	                        <span>Ghi chú</span>
	                        <textarea id="notesoftware" type="password"></textarea>
	                    </div>
	                    <input id="submitlogin" type="submit" value="Gửi yêu cầu">
	                </form>
	            </div>
	            <div class="clearfix"> </div>
	        </div>
	    </div>
        </div>
      </div>
    </div>
    <?php
    require dirname(__DIR__) . '/Block/footer.php';
    ?>
  </div>
</body>

</html>