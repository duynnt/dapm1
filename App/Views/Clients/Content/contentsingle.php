<style>
    #contactform textarea:focus {
        outline: none;
    }

    /* confirm dialog*/
    body {
        font-family: sans-serif
    }

    .dialog-ovelay {
        background-color: rgba(0, 0, 0, 0.836);
        position: fixed;
        width: 100%;
        height: 100%;
        z-index: 2000;
        top: 0;
        left: 0;
    }

    .dialog-ovelay .dialog {
        width: 400px;
        margin: 100px auto 0;
        background-color: #fff;
        box-shadow: 0 0 20px rgba(0, 0, 0, .2);
        border-radius: 3px;
        overflow: hidden
    }

    .dialog-ovelay .dialog header {
        padding: 10px 8px;
        background-color: #f6f7f9;
        border-bottom: 1px solid #e5e5e5
    }

    .dialog-ovelay .dialog header h3 {
        font-size: 14px;
        margin: 0;
        color: #555;
        display: inline-block
    }

    .dialog-ovelay .dialog header .fa-close {
        float: right;
        color: #c4c5c7;
        cursor: pointer;
        transition: all .5s ease;
        padding: 0 2px;
        border-radius: 1px
    }

    .dialog-ovelay .dialog header .fa-close:hover {
        color: #b9b9b9
    }

    .dialog-ovelay .dialog header .fa-close:active {
        box-shadow: 0 0 5px #673AB7;
        color: #a2a2a2
    }

    .dialog-ovelay .dialog .dialog-msg {
        padding: 12px 10px
    }

    .dialog-ovelay .dialog .dialog-msg p {
        margin: 0;
        font-size: 15px;
        color: #333
    }

    .dialog-ovelay .dialog footer {
        border-top: 1px solid #e5e5e5;
        padding: 8px 10px
    }

    .dialog-ovelay .dialog footer .controls {
        direction: rtl
    }

    .dialog-ovelay .dialog footer .controls .button {
        padding: 5px 15px;
        border-radius: 3px
    }

    .button {
        cursor: pointer
    }

    .button-default {
        background-color: rgb(248, 248, 248);
        border: 1px solid rgba(204, 204, 204, 0.5);
        color: #5D5D5D;
    }

    .button-danger {
        background-color: #f44336;
        border: 1px solid #d32f2f;
        color: #f5f5f5
    }

    .link {
        padding: 5px 10px;
        cursor: pointer
    }
</style>
<section class="item content">
    <div class="container toparea">
        <div class="underlined-title">
            <div class="editContent">
                <h1 class="text-center latestitems">Gửi Yêu Cầu</h1>
            </div>
            <div class="wow-hr type_short">
                <span class="wow-hr-h">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="done">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        Bạn đã giử yêu cầu thành công. Vui lòng chờ phản hồi
                    </div>
                </div>
                <div id="contactform">
                    <div class="form">
                        <textarea id="description" rows="7" placeholder="Mô tả yêu cầu: *"></textarea>
                        <textarea id="note" rows="3" placeholder="Ghi chú"></textarea>
                        <input onclick="sendrequest()" type="submit" id="submit" class="clearfix btn" value="Gửi Yêu Cầu">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- CALL TO ACTION =============================-->
<section class="content-block" style="background-color:#00bba7;">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="item" data-scrollreveal="enter top over 0.4s after 0.1s">
                    <h1 class="callactiontitle"> Liên hệ chúng tôi <span class="callactionbutton"><i class="fa fa-gift"></i> 24/7</span>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function sendrequest() {
        $.post("/dapm1/public/sendrequestorder", {
            description: $('#description').val(),
            note: $('#note').val()
        }, function(responsive) {
            if (responsive == 1) {
                Notification('Thông báo', 'Đã giử yêu cầu thành công', 'Đồng ý', );
            } else {
                Notification('Thông báo', 'Thất bại vui lòng thử lại sau !!', 'Thử lại', );
            }
        });
    }

    function Notification(title, msg, $false) {
        /*change*/
        var $content = "<div class='dialog-ovelay'>" +
            "<div class='dialog'><header>" +
            " <h3> " + title + " </h3> " +
            "<i class='fa fa-close'></i>" +
            "</header>" +
            "<div class='dialog-msg'>" +
            " <p> " + msg + " </p> " +
            "</div>" +
            "<footer>" +
            "<div class='controls'>" +
            " <button class='button button-default cancelAction'>" + $false + "</button> " +
            "</div>" +
            "</footer>" +
            "</div>" +
            "</div>";
        $('body').prepend($content);
        $('.cancelAction, .fa-close').click(function() {
            $(this).parents('.dialog-ovelay').fadeOut(500, function() {
                $(this).remove();
            });
        });

    }

    function Confirm(title, msg, $true, $false, $link) {
        /*change*/
        var $content = "<div class='dialog-ovelay'>" +
            "<div class='dialog'><header>" +
            " <h3> " + title + " </h3> " +
            "<i class='fa fa-close'></i>" +
            "</header>" +
            "<div class='dialog-msg'>" +
            " <p> " + msg + " </p> " +
            "</div>" +
            "<footer>" +
            "<div class='controls'>" +
            " <button class='button button-danger doAction'>" + $true + "</button> " +
            " <button class='button button-default cancelAction'>" + $false + "</button> " +
            "</div>" +
            "</footer>" +
            "</div>" +
            "</div>";
        $('body').prepend($content);
        $('.doAction').click(function() {
            window.open($link, "_blank"); /*new*/
            $(this).parents('.dialog-ovelay').fadeOut(500, function() {
                $(this).remove();
            });
        });
        $('.cancelAction, .fa-close').click(function() {
            $(this).parents('.dialog-ovelay').fadeOut(500, function() {
                $(this).remove();
            });
        });

    }
</script>