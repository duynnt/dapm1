
  <div class="cont">
    <div class="content">
      <div class="content-top-bottom">
        <h2>Sản Phẩm</h2>
        <div class="col-md-6 men">
          <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/t1.jpg" alt="">
            <div class="b-wrapper">
              <h3 class="b-animate b-from-top top-in b-delay03 ">
                <span>Quản Lý Khách Sạn</span>
              </h3>
            </div>
          </a>
          

        </div>
        <div class="col-md-6">
          <div class="col-md1 ">
            <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/t2.jpg" alt="">
              <div class="b-wrapper">
                <h3 class="b-animate b-from-top top-in1 b-delay03 ">
                  <span>Quản Lý Nhà Hàng</span>
                </h3>
              </div>
            </a>

          </div>
          <div class="col-md2">
            <div class="col-md-6 men1">
              <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/t3.jpg" alt="">
                <div class="b-wrapper">
                  <h3 class="b-animate b-from-top top-in2 b-delay03 ">
                    <span>Quản Lý Phòng Trọ</span>
                  </h3>
                </div>
              </a>

            </div>
            <div class="col-md-6 men2">
              <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/t4.jpg" alt="">
                <div class="b-wrapper">
                  <h3 class="b-animate b-from-top top-in2 b-delay03 ">
                    <span>Quản Lý ODO</span>
                  </h3>
                </div>
              </a>

            </div>
            <div class="clearfix"> </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      <div class="content-top">
        <h1>Sản Phẩm Mới</h1>
        <div class="grid-in">
          <div class="col-md-3 grid-top simpleCart_shelfItem">
            <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/pi.jpg" alt="">
              <div class="b-wrapper">
                <h3 class="b-animate b-from-left b-delay03 ">
                  <span>Quản Lý Khách Sạn</span>

                </h3>
              </div>
            </a>


            <p><a href="single.html">Quản Lý Khách Sạn</a></p>
            <a href="#" class="item_add">
              <p class="number item_price"><i> </i>500.000</p>
            </a>
          </div>
          <div class="col-md-3 grid-top simpleCart_shelfItem">
            <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/pi1.jpg" alt="">
              <div class="b-wrapper">
                <h3 class="b-animate b-from-left b-delay03 ">
                  <span>Quản Lý Nhà Ở</span>
                </h3>
              </div>
            </a>
            <p><a href="single.html">Quản Lý Nhà Ở</a></p>
            <a href="#" class="item_add">
              <p class="number item_price"><i> </i>500.000</p>
            </a>
          </div>
          <div class="col-md-3 grid-top simpleCart_shelfItem">
            <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/pi2.jpg" alt="">
              <div class="b-wrapper">
                <h3 class="b-animate b-from-left b-delay03 ">
                  <span>Hệ Thống Cafe</span>
                </h3>
              </div>
            </a>
            <p><a href="single.html">Hệ Thống Cafe</a></p>
            <a href="#" class="item_add">
              <p class="number item_price"><i> </i>500.000</p>
            </a>
          </div>
          <div class="col-md-3 grid-top">
            <a href="single.html" class="b-link-stripe b-animate-go thickbox"><img class="img-responsive" src="images/pi4.jpg" alt="">
              <div class="b-wrapper">
                <h3 class="b-animate b-from-left b-delay03 ">
                  <span>Quản Lý Bến Xe</span>
                </h3>
              </div>
            </a>
            <p><a href="single.html">Quản Lý Bến Xe</a></p>
            <a href="#" class="item_add">
              <p class="number item_price"><i> </i>500.000</p>
            </a>
          </div>
          <div class="clearfix"> </div>
        </div>
      </div>
    </div>
    <!----->
  </div>
  <!---->