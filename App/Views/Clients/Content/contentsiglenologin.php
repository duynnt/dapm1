	<!-- LATEST ITEMS =============================-->
	<section class="item content">
	    <div class="container">
	        <div class="underlined-title">
	            <div class="editContent">
	                <h1 class="text-center latestitems">Website mới</h1>
	            </div>
	            <div class="wow-hr type_short">
	                <span class="wow-hr-h">
	                    <i class="fa fa-star"></i>
	                    <i class="fa fa-star"></i>
	                    <i class="fa fa-star"></i>
	                </span>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-4">
	                <div class="productbox">
	                    <div class="fadeshop">
	                        <div class="text-center captionshop" style="display: none;">
	                            <h3>Sản phẩm</h3>
	                            <p>
	                                Đây là một đoạn trích ngắn để mô tả khái quát nội dung của mục đó.
	                            </p>
	                            <p>
	                                <a href="#" class="learn-more Chi tiếtlearn"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ hàng</a>
	                                <a href="#" class="learn-more Chi tiếtlearn"><i class="fa fa-link"></i> Chi tiết</a>
	                            </p>
	                        </div>
	                        <span class="maxproduct"><img src="newui/images/product1-1.jpg" alt=""></span>
	                    </div>
	                    <div class="product-Chi tiết">
	                        <a href="#">
	                            <h1>Web blog thân thiện</h1>
	                        </a>
	                        <span class="price">
	                            <span class="edd_price">4.900.000 vnd</span>
	                        </span>
	                    </div>
	                </div>
	            </div>
	            <!-- /.productbox -->
	            <div class="col-md-4">
	                <div class="productbox">
	                    <div class="fadeshop">
	                        <div class="text-center captionshop" style="display: none;">
	                            <h3>Sản phẩm</h3>
	                            <p>
	                                 Đây là một đoạn trích ngắn để mô tả khái quát nội dung của mục đó.
	                            </p>
	                            <p>
	                                <a href="#" class="learn-more Chi tiếtlearn"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ hàng</a>
	                                <a href="#" class="learn-more Chi tiếtlearn"><i class="fa fa-link"></i> Chi tiết</a>
	                            </p>
	                        </div>
	                        <span class="maxproduct"><img src="newui/images/product2.jpg" alt=""></span>
	                    </div>
	                    <div class="product-Chi tiết">
	                        <a href="#">
	                            <h1>Website thời trang</h1>
	                        </a>
	                        <span class="price">
	                            <span class="edd_price">5.900.000 vnd</span>
	                        </span>
	                    </div>
	                </div>
	            </div>
	            <!-- /.productbox -->
	            <div class="col-md-4">
	                <div class="productbox">
	                    <div class="fadeshop">
	                        <div class="text-center captionshop" style="display: none;">
	                            <h3>Sản phẩm</h3>
	                            <p>
	                                 Đây là một đoạn trích ngắn để mô tả khái quát nội dung của mục đó.
	                            </p>
	                            <p>
	                                <a href="#" class="learn-more Chi tiếtlearn"><i class="fa fa-shopping-cart"></i> Thêm vào giỏ hàng</a>
	                                <a href="#" class="learn-more Chi tiếtlearn"><i class="fa fa-link"></i> Chi tiết</a>
	                            </p>
	                        </div>
	                        <span class="maxproduct"><img src="newui/images/product2-3.jpg" alt=""></span>
	                    </div>
	                    <div class="product-Chi tiết">
	                        <a href="#">
	                            <h1>Website động sản</h1>
	                        </a>
	                        <span class="price">
	                            <span class="edd_price">1.900.000 vnd</span>
	                        </span>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    </div>
	</section>