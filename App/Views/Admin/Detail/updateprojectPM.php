<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php
    require dirname(__DIR__) . '/layout/headerlib.php';
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
</head>
<style>
    .addnewproject {
        float: left;
        margin-right: 140px;
    }

    input {
        margin-right: 30px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 8px;
        border: 1px solid #dee2e6;
    }

    th {
        height: 40px;
        text-align: left;
    }

    table.dataTable thead th,
    table.dataTable thead td {
        border-bottom: 1px solid #ccc;
    }

    table.dataTable.no-footer {
        border-bottom: 1px solid #ccc;
    }

    #table_id_filter input {
        background: #fff;
        margin: 0;
    }

    .requestorder .description p {
        text-align: justify;
        font-size: 17px;
    }

    .requestorder .content {
        padding: 10px 0;
    }

    .select2-container {
        width: 300px !important;
    }

    .requestorder .content input,
    textarea {
        width: 300px;
    }
</style>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
        require dirname(__DIR__) . '/layout/header.php';
        ?>
        <?php
        require dirname(__DIR__) . '/layout/sliderbarsofe.php';
        ?>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Quản Trị</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Cập nhật dự án</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7">
                        <!-- <div class="text-end upgrade-btn">
                <a href="https://www.wrappixel.com/templates/xtremeadmin/" class="text-white btn btn-danger"
                    target="_blank">Upgrade to Pro</a>
            </div> -->
                    </div>
                </div>
            </div>
            <div style="padding: 25px 20px 250px 20px;">
                <!-- grow -->
                <div class="sofeware requestorder">
                    <div class="container">
                        <div class="row">
                            <div class="text-center col-12">
                                <h3>Cập Nhật Dự Án Phần Mềm</h3>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Chọn dự án</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <select class="js-example-basic-single" name="state">
                                                    <option value="1">Dự án quản trị nhà hàng</option>
                                                    <option value="2">Dự án quản lý khách sạn</option>
                                                    <option value="3">Dự án thương mại điện tử</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Mã dự án:</h5>
                                            </div>
                                            <div class="col-4">
                                                <p>001</p>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Mã yêu cầu:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <select class="js-example-basic-single" name="state">
                                                    <option value="1">001</option>
                                                    <option value="2">002</option>
                                                    <option value="3">003</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Nhân viên phụ trách:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <p>Nguyễn Thanh Duy</p>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Loại dự án</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <select class="js-example-basic-single" name="state">
                                                    <option value="1">Dự án thực tế</option>
                                                    <option value="2">Dự án có sẵn</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Trạng thái</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <select class="js-example-basic-single" name="state">
                                                    <option value="1">Đang thực hiện</option>
                                                    <option value="2">Đã hoàn thành</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Thời gian thực hiện:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <p>6 tháng</p>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Ngày bắt đầu:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <input type="datetime" name="" id="">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Link sản phẩm:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <input type="text" name="" id="">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Bảo hành:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <input type="text" name="" id="">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Mô tả:</h5>
                                            </div>
                                            <div class="col-4 description">
                                                <textarea name="" id="" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="content row">
                                    <div class=" col-12">
                                        <a style="width:96px;float:right;color:#fff;margin-right:30px" type="button" class="btn btn-success">Lưu</a>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            require dirname(__DIR__) . '/layout/footer.php';
            ?>
        </div>
        <?php
        require dirname(__DIR__) . '/layout/footerlib.php';
        ?>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
        </script>
</body>

</html>