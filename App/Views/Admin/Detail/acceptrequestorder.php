<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
  <?php
  require dirname(__DIR__) . '/layout/headerlib.php';
  ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
</head>
<style>
  .addnewproject {
    float: left;
    margin-right: 140px;
  }

  input {
    margin-right: 30px;
  }

  table {
    width: 100%;
    border-collapse: collapse;
  }

  th,
  td {
    padding: 8px;
    border: 1px solid #dee2e6;
  }

  th {
    height: 40px;
    text-align: left;
  }

  table.dataTable thead th,
  table.dataTable thead td {
    border-bottom: 1px solid #ccc;
  }

  table.dataTable.no-footer {
    border-bottom: 1px solid #ccc;
  }

  #table_id_filter input {
    background: #fff;
    margin: 0;
  }

  .requestorder .description p {
    text-align: justify;
  }

  /* confirm dialog*/
  body {
    font-family: sans-serif
  }

  .dialog-ovelay {
    background-color: rgba(0, 0, 0, 0.836);
    position: fixed;
    width: 100%;
    height: 100%;
    z-index: 2000;
    top: 0;
    left: 0;
  }

  .dialog-ovelay .dialog {
    width: 400px;
    margin: 100px auto 0;
    background-color: #fff;
    box-shadow: 0 0 20px rgba(0, 0, 0, .2);
    border-radius: 3px;
    overflow: hidden
  }

  .dialog-ovelay .dialog header {
    padding: 10px 8px;
    background-color: #f6f7f9;
    border-bottom: 1px solid #e5e5e5
  }

  .dialog-ovelay .dialog header h3 {
    font-size: 14px;
    margin: 0;
    color: #555;
    display: inline-block
  }

  .dialog-ovelay .dialog header .fa-close {
    float: right;
    color: #c4c5c7;
    cursor: pointer;
    transition: all .5s ease;
    padding: 0 2px;
    border-radius: 1px
  }

  .dialog-ovelay .dialog header .fa-close:hover {
    color: #b9b9b9
  }

  .dialog-ovelay .dialog header .fa-close:active {
    box-shadow: 0 0 5px #673AB7;
    color: #a2a2a2
  }

  .dialog-ovelay .dialog .dialog-msg {
    padding: 12px 10px
  }

  .dialog-ovelay .dialog .dialog-msg p {
    margin: 0;
    font-size: 15px;
    color: #333
  }

  .dialog-ovelay .dialog footer {
    border-top: 1px solid #e5e5e5;
    padding: 8px 10px
  }

  .dialog-ovelay .dialog footer .controls {
    direction: rtl
  }

  .dialog-ovelay .dialog footer .controls .button {
    padding: 5px 15px;
    border-radius: 3px
  }

  .button {
    cursor: pointer
  }

  .button-default {
    background-color: rgb(248, 248, 248);
    border: 1px solid rgba(204, 204, 204, 0.5);
    color: #5D5D5D;
  }

  .button-danger {
    background-color: #f44336;
    border: 1px solid #d32f2f;
    color: #f5f5f5
  }

  .link {
    padding: 5px 10px;
    cursor: pointer
  }
</style>

<body>
  <div class="preloader">
    <div class="lds-ripple">
      <div class="lds-pos"></div>
      <div class="lds-pos"></div>
    </div>
  </div>
  <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <?php
    require dirname(__DIR__) . '/layout/header.php';
    ?>
    <?php
    require dirname(__DIR__) . '/layout/sliderbarsofe.php';
    ?>
    <div class="page-wrapper">
      <div class="page-breadcrumb">
        <div class="row align-items-center">
          <div class="col-5">
            <h4 class="page-title">Quản Trị</h4>
            <div class="d-flex align-items-center">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Duyệt yêu cầu</li>
                </ol>
              </nav>
            </div>
          </div>
          <div class="col-7">
            <!-- <div class="text-end upgrade-btn">
                <a href="https://www.wrappixel.com/templates/xtremeadmin/" class="text-white btn btn-danger"
                    target="_blank">Upgrade to Pro</a>
            </div> -->
          </div>
        </div>
      </div>
      <div style="padding: 25px 20px 250px 20px;">
        <!-- grow -->
        <div class="sofeware requestorder">
          <div class="container">
            <div class="row">
              <?php
              foreach ($datadetail as $key => $value) { ?>

                <div class="text-center col-12">
                  <h3>Yêu Cầu Làm Dự Án Phần Mềm</h3>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Người yêu cầu:</h5>
                  </div>
                  <div class="col-7 description">
                    <p>
                      <?php echo $value['name']; ?>
                    </p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Mã khách hàng:</h5>
                  </div>
                  <div class="col-7 description">
                    <p>
                      <?php echo $value['makh']; ?>
                    </p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Mô tả yêu cầu:</h5>
                  </div>
                  <div class="col-7">
                    <p style="text-align:justify;">
                      <?php echo $value['mota']; ?>
                    </p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Ngày gửi yêu cầu:</h5>
                  </div>
                  <div class="col-7 description">
                    <p><?php echo $value['ngaydat']; ?></p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2 ">
                    <h5>Trạng thái:</h5>
                  </div>
                  <div class="col-7 description">
                    <p class="status<?php echo $value['ma']; ?>"><?php echo $value['trangthai']; ?></p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Ghi chú:</h5>
                  </div>
                  <div class="col-7 description">
                    <p><?php echo $value['ghichu']; ?></p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Email:</h5>
                  </div>
                  <div class="col-7 description">
                    <p><?php echo $value['email']; ?></p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-1 col-2">
                    <h5>Số điện thoại:</h5>
                  </div>
                  <div class="col-7 description">
                    <p><?php echo $value['sdt']; ?></p>
                  </div>
                </div>
                <div class="content row">
                  <div class="offset-3 col-7">
                    <a onclick="accept(<?php echo $value['ma']; ?>)" style="width:96px;float:right;color:#fff" type="button" class="btn btn-success">duyệt ngay</a>
                  </div>

                </div>
              <?php
              }
              ?>

              <div class="clearfix"> </div>
            </div>
          </div>
        </div>

      </div>
      <?php
      require dirname(__DIR__) . '/layout/footer.php';
      ?>
    </div>
    <?php
    require dirname(__DIR__) . '/layout/footerlib.php';
    ?>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script>
      function accept(id) {
        $.post("/dapm1/public/dat-lam-phan-mem/chi-tiet/accept", {
          idycd: id,
        }, function(responsive) {
          if (responsive == 1) {
            Notification('Thông báo', 'Duyệt thành công !!', 'Quay lại', ); /*change*/
            $('.status' + id).html('đã xác nhận từ nhân viên phần mềm');
          } else {
            Notification('Thông báo', 'Duyệt thất bại !!', 'Thử lại', );
          }
        });
      }

      function Notification(title, msg, $false) {
        /*change*/
        var $content = "<div class='dialog-ovelay'>" +
          "<div class='dialog'><header>" +
          " <h3> " + title + " </h3> " +
          "<i class='fa fa-close'></i>" +
          "</header>" +
          "<div class='dialog-msg'>" +
          " <p> " + msg + " </p> " +
          "</div>" +
          "<footer>" +
          "<div class='controls'>" +
          " <button class='button button-default cancelAction'>" + $false + "</button> " +
          "</div>" +
          "</footer>" +
          "</div>" +
          "</div>";
        $('body').prepend($content);
        $('.cancelAction, .fa-close').click(function() {
          $(this).parents('.dialog-ovelay').fadeOut(500, function() {
            $(this).remove();
          });
        });

      }

      function Confirm(title, msg, $true, $false, $link) {
        /*change*/
        var $content = "<div class='dialog-ovelay'>" +
          "<div class='dialog'><header>" +
          " <h3> " + title + " </h3> " +
          "<i class='fa fa-close'></i>" +
          "</header>" +
          "<div class='dialog-msg'>" +
          " <p> " + msg + " </p> " +
          "</div>" +
          "<footer>" +
          "<div class='controls'>" +
          " <button class='button button-danger doAction'>" + $true + "</button> " +
          " <button class='button button-default cancelAction'>" + $false + "</button> " +
          "</div>" +
          "</footer>" +
          "</div>" +
          "</div>";
        $('body').prepend($content);
        $('.doAction').click(function() {
          window.open($link, "_blank"); /*new*/
          $(this).parents('.dialog-ovelay').fadeOut(500, function() {
            $(this).remove();
          });
        });
        $('.cancelAction, .fa-close').click(function() {
          $(this).parents('.dialog-ovelay').fadeOut(500, function() {
            $(this).remove();
          });
        });

      }
    </script>
</body>

</html>