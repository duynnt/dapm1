<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php
    require dirname(__DIR__) . '/layout/headerlib.php';
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">

</head>
<style>
    .addnewproject {
        float: left;
        margin-right: 140px;
    }

    input {
        margin-right: 30px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 8px;
        border: 1px solid #dee2e6;
    }

    th {
        height: 40px;
        text-align: left;
    }

    table.dataTable thead th,
    table.dataTable thead td {
        border-bottom: 1px solid #ccc;
    }

    table.dataTable.no-footer {
        border-bottom: 1px solid #ccc;
    }

    #table_id_filter input {
        background: #fff;
        margin: 0;
    }

    .requestorder .description p {
        text-align: justify;
        font-size: 17px;
    }

    .requestorder .content {
        padding: 10px 0;
    }

    .select2-container {
        width: 300px !important;
    }

    .requestorder .content input,
    textarea {
        width: 300px;
    }
</style>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
        require dirname(__DIR__) . '/layout/header.php';
        ?>
        <?php
        require dirname(__DIR__) . '/layout/sliderbarmarketing.php';
        ?>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Quản Trị</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Thêm mới dự án</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7">
                        <!-- <div class="text-end upgrade-btn">
                <a href="https://www.wrappixel.com/templates/xtremeadmin/" class="text-white btn btn-danger"
                    target="_blank">Upgrade to Pro</a>
            </div> -->
                    </div>
                </div>
            </div>
            <div style="padding: 25px 20px 250px 20px;">
                <!-- grow -->
                <div class="sofeware requestorder">
                    <div class="container">
                        <div class="row">
                            <div class="text-center col-12">
                                <h3>Thêm Mới Dự Án Phần Mềm</h3>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Mã dự án:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="number" name="" id="idproject">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Mã danh mục:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <select class="js-example-basic-single" id="categorys" name="state">
                                                    <option value="1">Dự án có sẵn</option>
                                                    <option value="2">Dự án thực tế</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Mã yêu cầu<spans style="color:red">*</span></h5>
                                            </div>
                                            <div class="col-8 description">
                                                <select class="js-example-basic-single" id="request" name="state">
                                                    <?php
                                                    foreach ($datagetrequest as $key => $value) { ?>
                                                        <option value="<?php echo $value['ma']; ?>"><?php echo $value['ten'] . ' - ' . $value['ma']; ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4 ">
                                                <h5>Tên nhân viên:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <p><?php echo $idnv; ?></p>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Tên dự án<spans style="color:red">*</span></h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="text" name="" id="nameproject">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Giá tiền:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="number" name="" id="price">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4 ">
                                                <h5>Ngày bắt đầu:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="datetime-local" id="meetingstart" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Thời gian thực hiện<spans style="color:red">*</span></h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="text" id="meetingdoing" />
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4 ">
                                                <h5>Link phần mềm:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="text" name="" id="linksf">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4 ">
                                                <h5>Mô tả:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <textarea name="" id="description" rows="6"></textarea>
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4 ">
                                                <h5>Bảo hành<spans style="color:red">*</span></h5>
                                            </div>
                                            <div class="col-8 description">
                                                <input type="text" name="" id="insurance">
                                            </div>
                                        </div>
                                        <div class="content row">
                                            <div class="col-4">
                                                <h5>Trạng thái:</h5>
                                            </div>
                                            <div class="col-8 description">
                                                <select class="js-example-basic-single" id="status" name="state">
                                                    <option value="Đang lên kế hoạch">Đang lên kế hoạch</option>
                                                    <option value="Đang thực hiện">Đang thực hiện</option>
                                                    <option value="Đã hoàn thành">Đã hoàn thành</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="content row">
                                    <div class="col-12">
                                        <a onclick="savedata()" style="width:96px;float:right;color:#fff;margin-right:30px;" type="button" class="btn btn-success">Lưu lại</a>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                    </div>
                </div>

            </div>
            <?php
            require dirname(__DIR__) . '/layout/footer.php';
            ?>
        </div>
        <?php
        require dirname(__DIR__) . '/layout/footerlib.php';
        ?>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function() {
                $('.js-example-basic-single').select2();
            });
            getdescriptions()
            $('#request').on('select2:select', async function(e) {
                getdescriptions();
            });

            function getdescriptions() {
                $.post("/dapm1/public/getdescriptions", {
                    idrequest: $('#request').val(),
                }, function(responsive) {
                    if (responsive != '') {
                        $('#description').text(responsive);
                    } else {
                        Notification('Thông báo', 'Có lỗi xảy ra !!', 'Thử lại', );
                    }
                });
            }

            function savedata() {
                if ($('#nameproject').val() == '' || $('#meetingdoing').val() == '' || $('#insurance').val() == '' || $('#request').val() == '') {
                    Notification('Thông báo', 'Vui lòng không để trống các trường !!', 'quay lại', );
                } else {
                    $.post("/dapm1/public/insertproject", {
                        idproject: $('#idproject').val(),
                        idcategory: $('#categorys').val(),
                        idrequest: $('#request').val(),
                        nameproject: $('#nameproject').val(),
                        price: $('#price').val(),
                        link: $('#linksf').val(),
                        description: $('#description').val(),
                        insurance: $('#insurance').val(),
                        status: $('#status').val(),
                        timestart: $('#meetingstart').val(),
                        timedoing: $('#meetingdoing').val(),
                    }, function(responsive) {
                        if (responsive == 1) {
                            Confirm('Thông báo', 'Thêm thành công !!', 'Đồng ý', 'Quay lại', '/dapm1/public/cap-nhat-du-an');
                            $('.status' + id).html('đã xác nhận từ nhân viên phần mềm');
                        } else {
                            Notification('Thông báo', 'Thêm thất bại vui lòng thử lại sau !!', 'Thử lại', );
                        }
                    });
                }
            }
        </script>
</body>

</html>