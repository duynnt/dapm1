<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php
        require dirname(__DIR__) . '/layout/headerlib.php';
    ?>
</head>
<style>
    .addnewproject {
        float: left;
        margin-right: 140px;
    }

    input {
        margin-right: 30px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 8px;
        border: 1px solid #dee2e6;
    }

    th {
        height: 40px;
        text-align: left;
    }
</style>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
        require dirname(__DIR__) . '/layout/header.php';
        ?>
        <?php
        require dirname(__DIR__) . '/layout/sliderbar.php';
        ?>
        <div class="page-wrapper">
            <?php
            require dirname(__DIR__) . '/layout/headerpage.php';
            ?>
            <div style="margin: 50px 0px 250px 50px;">
                
            </div>
            <?php
            require dirname(__DIR__) . '/layout/footer.php';
            ?>
        </div>
    </div>
    <?php
    require dirname(__DIR__) . '/layout/footerlib.php';
    ?>
</body>

</html>