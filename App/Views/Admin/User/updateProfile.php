<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php
        require dirname(__DIR__).'/layout/headerlib.php';
    ?>
</head>
<style>
.addnewproject {
    float: left;
    margin-right: 140px;
}

input {
    margin-right: 30px;
}

table {
    width: 100%;
    border-collapse: collapse;
}

th,
td {
    padding: 8px;
    border: 1px solid #dee2e6;
}

th {
    height: 40px;
    text-align: left;
}
</style>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
            require dirname(__DIR__). '/layout/header.php';
        ?>
        <?php
            require dirname(__DIR__). '/layout/sliderbar.php';
        ?>
        <div class="page-wrapper">
            <?php
                require dirname(__DIR__). '/layout/headerpage.php';
            ?>
            <div style="margin: 25px 20px 250px 20px;">
                <div class="grow">
                    <div class="container">
                        <h2>Danh Sách User</h2>
                    </div>
                </div>
                <!-- grow -->
                <div class="sofeware">
                    <div class="container">
                        <!-- <div class="addnewproject">
                            <button type="button" class="btn btn-primary">Thêm mới</button>
                        </div> -->
                        <table id="table_id" class="display">
                            <thead>
                                <tr role="row">
                                    <th>STT</th>
                                    <th>Tên User</th>
                                    <th>Phân quyền</th>
                                    <th>Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>QuangQui</td>
                                    <td>
                                        0<input type="checkbox" name="" id="">
                                        1<input type="checkbox" name="" id="">
                                        2<input type="checkbox" name="" id="">
                                        3<input type="checkbox" name="" id="">
                                    </td>
                                    <td class="action">
                                        <button type="button" class="btn btn-primary">Xóa</button>
                                        <button type="button" class="btn btn-success">Lưu</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>ThanhDuy</td>
                                    <td>
                                        0<input type="checkbox" name="" id="">
                                        1<input type="checkbox" name="" id="">
                                        2<input type="checkbox" name="" id="">
                                        3<input type="checkbox" name="" id="">
                                    </td>
                                    <td class="action">
                                        <button type="button" class="btn btn-primary">Xóa</button>
                                        <button type="button" class="btn btn-success">Lưu</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Khoa</td>
                                    <td>
                                        0<input type="checkbox" name="" id="">
                                        1<input type="checkbox" name="" id="">
                                        2<input type="checkbox" name="" id="">
                                        3<input type="checkbox" name="" id="">
                                    </td>
                                    <td class="action">
                                        <button type="button" class="btn btn-primary">Xóa</button>
                                        <button type="button" class="btn btn-success">Lưu</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Vinh</td>
                                    <td>
                                        0<input type="checkbox" name="" id="">
                                        1<input type="checkbox" name="" id="">
                                        2<input type="checkbox" name="" id="">
                                        3<input type="checkbox" name="" id="">
                                    </td>
                                    <td class="action">
                                        <button type="button" class="btn btn-primary">Xóa</button>
                                        <button type="button" class="btn btn-success">Lưu</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <?php
                require dirname(__DIR__). '/layout/footer.php';
            ?>
        </div>
    </div>
    <?php
        require dirname(__DIR__). '/layout/footerlib.php';
    ?>
</body>

</html>