<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown m-t-20">
                        <div class="user-pic"><img src="https://i.pinimg.com/736x/5f/40/6a/5f406ab25e8942cbe0da6485afd26b71.jpg" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu m-l-10">
                            <a href="#" class="" id="Userdd" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <h5 class="font-medium m-b-0 user-name">Nhân Viên Marketing
                                </h5>
                                <span class="op-5 user-email">duynguyen@gmailmail</span>
                            </a>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>
                <!-- User Profile-->
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="/dapm1/public/duyet-yeu-cau-don-dat-hang" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Duyệt yêu cầu đơn đặt hàng</span></a></li>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="/dapm1/public/cap-nhat-du-an" aria-expanded="false"><i class="mdi mdi-border-all"></i><span class="hide-menu">Cập nhật dự án</span></a>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="/dapm1/public/khach-hang" aria-expanded="false"><i class="mdi mdi-border-all"></i><span class="hide-menu">Xem thông tin khách hàng</span></a>
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark" href="/dapm1/public/logout" aria-expanded="false"><i class="fa fa-power-off m-r-5 m-l-5"></i><span class="hide-menu">Đăng xuất</span></a>
                </li>
            </ul>

        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>