<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php
    require dirname(__DIR__) . '/layout/headerlib.php';
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
</head>
<style>
    .addnewproject {
        float: left;
        margin-right: 140px;
    }

    input {
        margin-right: 30px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 8px;
        border: 1px solid #dee2e6;
    }

    th {
        height: 40px;
        text-align: left;
    }

    table.dataTable thead th,
    table.dataTable thead td {
        border-bottom: 1px solid #ccc;
    }

    table.dataTable.no-footer {
        border-bottom: 1px solid #ccc;
    }
</style>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
        require dirname(__DIR__) . '/layout/header.php';
        ?>
        <?php
        require dirname(__DIR__) . '/layout/sliderbarmarketing.php';
        ?>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Quản Trị</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Danh mục</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7">
                        <!-- <div class="text-end upgrade-btn">
                <a href="https://www.wrappixel.com/templates/xtremeadmin/" class="text-white btn btn-danger"
                    target="_blank">Upgrade to Pro</a>
            </div> -->
                    </div>
                </div>
            </div>
            <div style="margin: 25px 20px 250px 20px;">
                <!-- grow -->
                <div class="sofeware">
                
                </div>
            </div>
            <?php
            require dirname(__DIR__) . '/layout/footer.php';
            ?>
        </div>
    </div>
    <?php
    require dirname(__DIR__) . '/layout/footerlib.php';
    ?>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#table_id').DataTable({
                "lengthMenu": [10, 50, 100, 500, 1000, 5000],
                "language": {
                    "sLengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
                    "sZeroRecords": "Không tìm thấy dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "sInfoEmpty": "Không có dữ liệu nào",
                    "sInfoFiltered": "(được lọc từ tổng sô _MAX_ trong dữ liệu)",
                    "sSearch": "Tìm kiếm:",
                    "oPaginate": {
                        "sNext": "Sau",
                        "sPrevious": "Trước"
                    },
                }
            });
        });
    </script>
</body>

</html>