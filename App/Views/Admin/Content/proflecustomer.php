<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <?php
    require dirname(__DIR__) . '/layout/headerlib.php';
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
</head>
<style>
    .addnewproject {
        float: left;
        padding-bottom: 30px;
    }

    input {
        margin-right: 30px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 8px;
        border: 1px solid #dee2e6;
    }

    th {
        height: 40px;
        text-align: left;
    }

    #table_id_filter input {
        margin-right: 0px;
    }

    table.dataTable thead th,
    table.dataTable thead td {
        border-bottom: 1px solid #ccc;
    }

    table.dataTable.no-footer {
        border-bottom: 1px solid #ccc;
    }

    #table_id_filter input {
        background: #fff;
        margin: 0;
    }
</style>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        <?php
        require dirname(__DIR__) . '/layout/header.php';
        ?>
        <?php
        require dirname(__DIR__) . '/layout/sliderbarmarketing.php';
        ?>
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">Quản Trị</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Cập nhật dự án</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7">
                        <!-- <div class="text-end upgrade-btn">
                <a href="https://www.wrappixel.com/templates/xtremeadmin/" class="text-white btn btn-danger"
                    target="_blank">Upgrade to Pro</a>
            </div> -->
                    </div>
                </div>
            </div>
            <div style="margin: 25px 20px 250px 20px;">
                <!-- grow -->
                <div class="sofeware">
                    <div class="container">
                        <table id="table_id" class="display">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tài khoản</th>
                                    <th>Tên khách hàng</th>
                                    <th>CMND</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                                    <?php
                                    $stt = 0;
                                        foreach ($profile as $key => $value) {$stt +=1;?>
                                         <tr class="">
                                            <td><?php echo $stt ?></td>
                                            <td><?php echo $value['taikhoan'] ?></td>
                                            <td><?php echo $value['ten'] ?></td>
                                            <td><?php echo $value['cmnd'] ?></td>
                                            <td><?php echo $value['email'] ?></td>
                                            <td><?php echo $value['sdt'] ?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    

                                
                            </tbody>
                        </table>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <?php
            require dirname(__DIR__) . '/layout/footer.php';
            ?>
        </div>
    </div>
    <?php
    require dirname(__DIR__) . '/layout/footerlib.php';
    ?>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#table_id').DataTable({
                "lengthMenu": [10, 50, 100, 500, 1000, 5000],
                "language": {
                    "sLengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
                    "sZeroRecords": "Không tìm thấy dữ liệu",
                    "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
                    "sInfoEmpty": "Không có dữ liệu nào",
                    "sInfoFiltered": "(được lọc từ tổng sô _MAX_ trong dữ liệu)",
                    "sSearch": "Tìm kiếm:",
                    "oPaginate": {
                        "sNext": "Sau",
                        "sPrevious": "Trước"
                    },
                }
            });
        });
        
        function remover(id) {
            Confirmremover('Thông báo', 'Bạn có chắc chắn muốn xóa!!','Đồng ý', 'Quay lại', id);
        }
        function Confirmremover(title, msg, $true, $false, $link) {
            /*change*/
            var $content = "<div class='dialog-ovelay'>" +
                "<div class='dialog'><header>" +
                " <h3> " + title + " </h3> " +
                "<i class='fa fa-close'></i>" +
                "</header>" +
                "<div class='dialog-msg'>" +
                " <p> " + msg + " </p> " +
                "</div>" +
                "<footer>" +
                "<div class='controls'>" +
                " <button class='button button-danger doAction'>" + $true + "</button> " +
                " <button class='button button-default cancelAction'>" + $false + "</button> " +
                "</div>" +
                "</footer>" +
                "</div>" +
                "</div>";
            $('body').prepend($content);
            $('.doAction').click(function() {
                $.post("/dapm1/public/removerproject", {
                    idduan: $link,
                }, function(responsive) {
                    if (responsive == 1) {
                        Notification('Thông báo', 'Xóa thành công!!', 'Đồng ý', );
                        $('.dataproject'+$link).remove();
                    } else {
                        Notification('Thông báo', 'Thất bại vui lòng thử lại sau !!', 'Thử lại', );
                    }
                }); 
                $(this).parents('.dialog-ovelay').fadeOut(500, function() {
                    $(this).remove();
                });
            });
            $('.cancelAction, .fa-close').click(function() {
                $(this).parents('.dialog-ovelay').fadeOut(500, function() {
                    $(this).remove();
                });
            });

        }
    </script>
</body>

</html>