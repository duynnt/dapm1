<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class SoftWares extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */

    public static function insertduan($idproject,$idcategory,$idnv,$idrequest,$nameproject,$price,$description,$timestart,$timedoing,$link,$insurance,$status){
        $db = static::getDB(); 
        $date = date_create($timestart);
        $dates = date_format($date, 'Y/m/d H:i:s'); 
        if ($idproject == ''){
            $sql = "INSERT INTO duan (`MaDM`, `MaNV`, `MaYC`, `TenDA`, `GiaTien`, `MoTa`, `NgayBatDau`, `TGThucHien`, `LinkSanPham`, `BaoHanh`, `TrangThai`)
            VALUES ($idcategory,$idnv,$idrequest,'$nameproject','$price','$description','$dates','$timedoing','$link','$insurance','$status')";
        }else{
            $sql = "INSERT INTO duan (`MaDA`, `MaDM`, `MaNV`, `MaYC`, `TenDA`, `GiaTien`, `MoTa`, `NgayBatDau`, `TGThucHien`, `LinkSanPham`, `BaoHanh`, `TrangThai`)
            VALUES ($idproject,$idcategory,$idnv,$idrequest,'$nameproject','$price','$description','$dates','$timedoing','$link','$insurance','$status')";
        }
        if (mysqli_query($db,$sql)){
            $slqupdate = "UPDATE duanyeucau
            SET TrangThai = N'Đã xác nhận'
            WHERE MaYC = ".$idrequest;
            mysqli_query($db,$slqupdate);
            return 1;
        }else{
            return 0;
        }
    }
    public static function updateduan($idproject,$idcategory,$idnv,$idrequest,$nameproject,$price,$description,$timestart,$timedoing,$link,$insurance,$status){
        $db = static::getDB(); 
        $date = date_create($timestart);
        $dates = date_format($date, 'Y/m/d H:i:s'); 
        $select = "SELECT * FROM duan WHERE MaDA=$idproject";
        $result = mysqli_query($db,$select);
        $tmp = 0;
        while (($rows = mysqli_fetch_assoc($result))!=null){
            if ($rows['TrangThai'] == 'Đã hoàn thành'){
                $tmp = 1;
            }
        }
        if ($tmp == 1){
            return 0; 
        }
        $sql = "UPDATE duan SET 
        MaDM = $idcategory,
        MaNV = $idnv,  
        MaYC = $idrequest, 
        TenDA = '$nameproject', 
        GiaTien = $price, 
        NgayBatDau = '$dates', 
        TGThucHien = '$timedoing', 
        LinkSanPham = '$link', 
        BaoHanh = '$insurance', 
        TrangThai = '$status' 
        WHERE
        MaDA = $idproject";
        if (mysqli_query($db,$sql)){
            if ($status == 'Đang lên kế hoạch' || $status == 'Đang thực hiện'){
                $slqupdate = "UPDATE duanyeucau SET TrangThai = 'Đã xác nhận' WHERE MaYC=".$idrequest;
                mysqli_query($db,$slqupdate);
            }else{
                $slqupdate = "UPDATE duanyeucau SET TrangThai = 'Đã hoàn thành' WHERE MaYC=".$idrequest;
                mysqli_query($db,$slqupdate);
            }
            return 1;
        }else{
            return 0;
        }
    }
    public static function getdatarequest(){
        $db = static::getDB();
        $sql = "SELECT * FROM duan";
        $result = mysqli_query($db,$sql);
        $list = [];
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $list[$rows['MaDA']]['name'] = $rows['TenDA'];
            $list[$rows['MaDA']]['price'] = $rows['GiaTien'];
            $list[$rows['MaDA']]['link'] = $rows['LinkSanPham'];
            $list[$rows['MaDA']]['trangthai'] = $rows['TrangThai'];
            $list[$rows['MaDA']]['mada'] = $rows['MaDA'];
        }       
        return $list;
    }
    public static function getdataprofilerequest(){
        $db = static::getDB();
        $sql = "SELECT * FROM khachhang";
        $result = mysqli_query($db,$sql);
        $list = [];
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $sqlnv = "SELECT * FROM taikhoan";
            $resultnv = mysqli_query($db,$sqlnv);
            while (($rowsnv = mysqli_fetch_assoc($resultnv))!=null){
                if ($rowsnv['TaiKhoan'] == $rows['TaiKhoan']){
                    if ($rowsnv['Quyen'] == 3){
                        $list[$rows['MaKH']]['taikhoan'] = $rows['TaiKhoan'];
                        $list[$rows['MaKH']]['ten'] = $rows['TenKH'];
                        $list[$rows['MaKH']]['cmnd'] = $rows['CMND'];
                        $list[$rows['MaKH']]['email'] = $rows['Email'];
                        $list[$rows['MaKH']]['sdt'] = $rows['SDT'];
                    }
                }
            }
        }       
        return $list;
    }
    public static function getdetailproject($id){
        $db = static::getDB();
        $sql = "SELECT * FROM duan WHERE MaDA=".$id;
        $result = mysqli_query($db,$sql);
        $list = [];
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $list[$rows['MaDA']]['name'] = $rows['TenDA'];
            $list[$rows['MaDA']]['price'] = $rows['GiaTien'];
            $list[$rows['MaDA']]['link'] = $rows['LinkSanPham'];
            $list[$rows['MaDA']]['trangthai'] = $rows['TrangThai'];
            $list[$rows['MaDA']]['mada'] = $rows['MaDA'];
            $list[$rows['MaDA']]['madm'] = $rows['MaDM'];
            
            $sqlnv = "SELECT * FROM nhanvien WHERE MaNV=".$rows['MaNV'];
            $resultnv = mysqli_query($db,$sqlnv);
            while (($rownv = mysqli_fetch_assoc($resultnv))!=null){
                $list[$rows['MaDA']]['tennv'] = $rownv['HoTen'];
            }
            $list[$rows['MaDA']]['mayc'] = $rows['MaYC'];
            $list[$rows['MaDA']]['mota'] = $rows['MoTa'];
            $list[$rows['MaDA']]['ngaybatdau'] = $rows['NgayBatDau'];
            $list[$rows['MaDA']]['tgthien'] = $rows['TGThucHien'];
            $list[$rows['MaDA']]['baohanh'] = $rows['BaoHanh'];
            
        }       
        return $list;
    }
    public static function removeduan($idproject){
        $db = static::getDB(); 
        $sqlelect = "SELECT * FROM duan WHERE MaDA=".$idproject;
        $result = mysqli_query($db,$sqlelect);
        $idyc=0;
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $idyc = $rows['MaYC'];
        }
        $sql = "DELETE FROM duan WHERE MaDA =".$idproject;
        if (mysqli_query($db,$sql)){
            $slqupdate = "UPDATE duanyeucau SET TrangThai= 'Đã hủy bỏ' WHERE MaYC=".$idyc;
            mysqli_query($db,$slqupdate);
            return 1;
        }else{
            return 0;
        }
    }
    public static function finishduan($idproject){
        $db = static::getDB(); 
        $sqlelect = "SELECT * FROM duan WHERE MaDA=".$idproject;
        $result = mysqli_query($db,$sqlelect);
        $idyc=0;
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $idyc = $rows['MaYC'];
        }
        $sql = "UPDATE duan SET TrangThai = 'Đã hoàn thành'  WHERE MaDA =".$idproject;
        if (mysqli_query($db,$sql)){
            $slqupdate = "UPDATE duanyeucau SET TrangThai= 'Đã hoàn thành' WHERE MaYC=".$idyc;
            mysqli_query($db,$slqupdate);
            return 1;
        }else{
            return 0;
        }
    }
    public static function sendrequest($description,$note,$makh){
        $db = static::getDB(); 
        $date = date('Y-m-d');
        $sql = "SELECT * FROM khachhang WHERE TaiKhoan='$makh'";
        $result = mysqli_query($db,$sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $id = $rows['MaKH'];
        }
        $sqlelect = "INSERT INTO duanyeucau (MaKH, MoTa, NgayDat, GhiChu) 
        VALUES ('$id','$description','$date','$note')";
        if (mysqli_query($db,$sqlelect)){
            return 1;
        }else{
            return 0;
        }
    }
    public static function getdescriptions($idrequest){
        $db = static::getDB(); 
        $sql = "SELECT * FROM duanyeucau WHERE MaYC='$idrequest'";
        $result = mysqli_query($db,$sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $mota = $rows['MoTa'];
        }
        return $mota;
    }
    
}
