<?php

namespace App\Models;

use PDO;


/**
 * Example user model
 *
 * PHP version 7.0
 */
class Products extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAllProduct()
    {
        $mang = array();
        $db = static::getDB();
        $sql = "SELECT * FROM products";
        $stmt = mysqli_query($db, $sql);      
        while ($rows = mysqli_fetch_array($stmt)) {
            $mang[] = $rows;
        }
        return json_encode($mang);
    }
    public static function getAllCategory(){
        $mang = array();
        $db = static::getDB();
        $sql = "SELECT * FROM categorys";
        $stmt = mysqli_query($db, $sql);      
        while ($rows = mysqli_fetch_array($stmt)) {
            $mang[] = $rows;
        }
        return json_encode($mang);
    }
    public static function getProductID($id){    
        $db = static::getDB();
        $sql = "SELECT * FROM products WHERE ID = '$id'";
        $stmt = mysqli_query($db, $sql);      
        while ($rows = mysqli_fetch_array($stmt)) {
            $mang[] = $rows;
        }
        return json_encode($mang);
    }
}