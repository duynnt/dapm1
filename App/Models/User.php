<?php

namespace App\Models;

use PDO;

/**
 * Example user model
 *
 * PHP version 7.0
 */
class User extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM users');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }    
    public static function register($user,$pass){
        $db = static::getDB();  
        $pass = md5($pass);
        $sql = "INSERT INTO taikhoan VALUES ('$user','$pass',3)";
        if (mysqli_query($db,$sql)){
            return 1;
        }else{
            return 0;
        }
    }
    public static function checklogin($user,$pass){
        $db = static::getDB();
        $sql = "SELECT * FROM taikhoan";
        $result = mysqli_query($db,$sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            if ($rows['TaiKhoan'] == $user && $rows['MatKhau'] == md5($pass)){
                return 1;
            }
        }
        return 0;
    }
    public static function getrules($user){
        $db = static::getDB();
        $sql = "SELECT * FROM taikhoan";
        $result = mysqli_query($db,$sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            if ($rows['TaiKhoan'] == $user){
                return $rows['Quyen'];
            }
        }
        return -1;
    }
    public static function getidnv($user){
        $db = static::getDB();
        $sql = "SELECT * FROM nhanvien";
        $result = mysqli_query($db,$sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            if ($rows['TaiKhoan'] == $user){
                return $rows['HoTen'];
            }
        }
        return -1;
    }
    public static function getidnreal($user){
        $db = static::getDB();
        $sql = "SELECT * FROM nhanvien";
        $result = mysqli_query($db,$sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            if ($rows['TaiKhoan'] == $user){
                return $rows['MaNV'];
            }
        }
        return -1;
    }
    public static function getRequest(){
        $db = static::getDB();
        $sql = "SELECT * FROM duanyeucau ORDER BY MaYC DESC";
        $result = mysqli_query($db,$sql);
        $list = [];
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $sqlnv = "SELECT * FROM khachhang WHERE MaKH=".$rows['MaKH'];
            $resultnv = mysqli_query($db,$sqlnv);
            while (($rownv = mysqli_fetch_assoc($resultnv))!=null){
                $list[$rows['MaYC']]['name'] = $rownv['TenKH'];
                $list[$rows['MaYC']]['mota'] = $rows['MoTa'];
                $list[$rows['MaYC']]['ngaydat'] = $rows['NgayDat'];
                $list[$rows['MaYC']]['ghichu'] = $rows['GhiChu'];
                $list[$rows['MaYC']]['trangthai'] = $rows['TrangThai'];
                $list[$rows['MaYC']]['ma'] = $rows['MaYC'];
            }
        }       
        return $list;
    }
    public static function getRequestID($id){
        $db = static::getDB();
        $sql = "SELECT * FROM duanyeucau WHERE MaYC=".$id;
        $result = mysqli_query($db,$sql);
        $list = [];
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $sqlnv = "SELECT * FROM khachhang WHERE MaKH=".$rows['MaKH'];
            $resultnv = mysqli_query($db,$sqlnv);
            while (($rownv = mysqli_fetch_assoc($resultnv))!=null){
                $list[$rows['MaYC']]['name'] = $rownv['TenKH'];
                $list[$rows['MaYC']]['mota'] = $rows['MoTa'];
                $list[$rows['MaYC']]['ngaydat'] = $rows['NgayDat'];
                $list[$rows['MaYC']]['ghichu'] = $rows['GhiChu'];
                $list[$rows['MaYC']]['trangthai'] = $rows['TrangThai'];
                $list[$rows['MaYC']]['ma'] = $rows['MaYC'];
                $list[$rows['MaYC']]['makh'] = $rownv['MaKH'];
                $list[$rows['MaYC']]['email'] = $rownv['Email'];
                $list[$rows['MaYC']]['sdt'] = $rownv['SDT'];
            }
        }       
        return $list;
    }
    public static function acceptorderrequest($id){
        $db = static::getDB();
        $sql = "UPDATE duanyeucau SET TrangThai= N'đã xác nhận từ nhân viên marketing' WHERE MaYC=".$id;
        if (mysqli_query($db,$sql)){
            return 1;
        }else{
            return 0;
        }
    }
    public static function getRequestPM(){
        $db = static::getDB();
        $sql = "SELECT * FROM duanyeucau WHERE TrangThai = N'đã xác nhận từ nhân viên marketing' OR TrangThai = N'đã xác nhận từ nhân viên phần mềm'";
        $result = mysqli_query($db,$sql);
        $list = [];
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $sqlnv = "SELECT * FROM khachhang WHERE MaKH=".$rows['MaKH'];
            $resultnv = mysqli_query($db,$sqlnv);
            while (($rownv = mysqli_fetch_assoc($resultnv))!=null){
                $list[$rows['MaYC']]['name'] = $rownv['TenKH'];
                $list[$rows['MaYC']]['mota'] = $rows['MoTa'];
                $list[$rows['MaYC']]['ngaydat'] = $rows['NgayDat'];
                $list[$rows['MaYC']]['ghichu'] = $rows['GhiChu'];
                $list[$rows['MaYC']]['trangthai'] = $rows['TrangThai'];
                $list[$rows['MaYC']]['ma'] = $rows['MaYC'];
            }
        }       
        return $list;
    }
    public static function acceptorderrequestPM($id){
        $db = static::getDB();
        $sql = "UPDATE duanyeucau SET TrangThai= N'đã xác nhận từ nhân viên phần mềm' WHERE MaYC=".$id;
        if (mysqli_query($db,$sql)){
            return 1;
        }else{
            return 0;
        }
    }
    
}
