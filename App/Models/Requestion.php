<?php

namespace App\Models;


/**
 * Example user model
 *
 * PHP version 7.0
 */
class Requestion extends \Core\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */   
    public static function getrequest()
    {
        $data = Array();
        $db = static::getDB();
        $sql = "SELECT * FROM duanyeucau WHERE TrangThai = N'đã xác nhận từ nhân viên phần mềm'";
        $result = mysqli_query($db, $sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $slqkh = "SELECT * FROM khachhang WHERE MaKH =".$rows['MaKH'];
            $resultkh = mysqli_query($db, $slqkh);
            while (($rowkh = mysqli_fetch_assoc($resultkh))!=null){
                $data[$rows['MaYC']]['ma'] = $rows['MaYC'];
                $data[$rows['MaYC']]['ten'] = $rowkh['TenKH'];
            }
        }
        return $data;
    }
    public static function getrequestvs()
    {
        $data = Array();
        $db = static::getDB();
        $sql = "SELECT * FROM duanyeucau WHERE TrangThai = N'Đã xác nhận'";
        $result = mysqli_query($db, $sql);
        while (($rows = mysqli_fetch_assoc($result))!=null){
            $slqkh = "SELECT * FROM khachhang WHERE MaKH =".$rows['MaKH'];
            $resultkh = mysqli_query($db, $slqkh);
            while (($rowkh = mysqli_fetch_assoc($resultkh))!=null){
                $data[$rows['MaYC']]['ma'] = $rows['MaYC'];
                $data[$rows['MaYC']]['ten'] = $rowkh['TenKH'];
            }
        }
        return $data;
    }
    
}