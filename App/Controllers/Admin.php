<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\SoftWares;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Admin extends \Core\Controller
{

    public function indexAction() {
        View::render('Admin/Home/index.php');
    }
    public function updateProfileAction() {
        View::render('Admin/User/updateProfile.php');
    }
    public function homeAction() {
        $data = User::getRequest();
        View::render('Admin/Content/browserequest.php',['dataYC'=>$data]);
    }
    public function updateprojectAction() {
        $data = SoftWares::getdatarequest();
        View::render('Admin/Content/controlsoftware.php',['duan'=>$data]);
    }
    public function profileAction() {
        $data = SoftWares::getdataprofilerequest();
        View::render('Admin/Content/proflecustomer.php',['profile'=>$data]);
    }
    public function browrequetsAction() {
        $data = SoftWares::getdatarequest();
        View::render('Admin/Content/updateproject.php',['duan'=>$data]);
    }
    public function assetorderAction() {
        $data = User::getRequestPM();
        View::render('Admin/Content/browse-request.php',['dataYC'=>$data]);
    }
    public function logoutAction() {
        unset($_SESSION['login']);
        View::render('Clients/Home/indexlogin.php');
    }
    public function adminmanagermentAction() {
        if(isset($_SESSION['login'])){
            if ($_SESSION['login']['rules'] == 0){
                View::render('Admin/User/updateProfile.php');
            }else{
                if ($_SESSION['login']['rules'] == 1){
                    $data = User::getRequestPM();
                    View::render('Admin/Content/browse-request.php',['dataYC'=>$data]);
                }else{
                    $data = User::getRequest();
                    View::render('Admin/Content/browserequest.php',['dataYC'=>$data]);
                }
            }
        }else{
            View::render('Clients/Home/index.php');
        }
    }
    public function updateinfoAction() {
        View::render('Admin/User/updateinfo_sofeware.php');
    }
    public function updatecontactAction() {
        View::render('Admin/Contact/updatecontact.php');
    }
    public function updatesofewareAction() {
        View::render('Admin/Sofeware/updatesofeware.php');
    }
    public function updatecategoryAction() {
        View::render('Admin/Category/updatecategory.php');
    }
    public function updateinfomtAction() {
        View::render('Admin/User/updateinfomt.php');
    }
    
}
