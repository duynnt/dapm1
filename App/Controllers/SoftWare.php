<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\SoftWares;
use App\Models\Requestion;
/**
 * Home controller
 *
 * PHP version 7.0
 */
class SoftWare extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function detailordersoftwareAction()
    {
        $params = $this->route_params;
        $data = User::getRequestID($params['idrequest']);
        View::render('Admin/Detail/acceptrequestorder.php',['datadetail' => $data]);
    }
    public function updatePMAction()
    {
        // $params = $this->route_params;
        // var_dump($params);
        // die();
        View::render('Admin/Detail/updateprojectPM.php');
    }
    public function editprojectAction()
    {
        $params = $this->route_params;
        $data = SoftWares::getdetailproject($params['idrequest']);
        $getrequest = Requestion::getrequestvs();
        View::render('Admin/Detail/editproject.php',['duan'=>$data,'datagetrequest'=>$getrequest]);
    }
    
    public function addnewprojectAction()
    {
        $getrequest = Requestion::getrequest();
        View::render('Admin/Detail/addnewproject.php',['datagetrequest'=>$getrequest,'idnv'=>$_SESSION['login']['manv']]);
    }
    public function acceptmketingAction()
    {
        $params = $this->route_params;
        $data = User::getRequestID($params['idrequest']);
        View::render('Admin/Detail/acceptmketing.php',['datadetail' => $data]);
    }
    public function acceptordermketingAction()
    { 
        echo User::acceptorderrequest($_POST['idycd']);
    }
    public function acceptordersoftwareAction()
    {
        echo User::acceptorderrequestPM($_POST['idycd']);
    }
    public function insertsoftwareAction()
    {
        echo SoftWares::insertduan($_POST['idproject'],$_POST['idcategory'],$_SESSION['login']['maid'],$_POST['idrequest'],$_POST['nameproject'],$_POST['price'],$_POST['description'],$_POST['timestart'],$_POST['timedoing'],$_POST['link'],$_POST['insurance'],$_POST['status']);
    }
    public function updatesoftwarecontrolAction()
    {
        echo SoftWares::updateduan($_POST['idproject'],$_POST['idcategory'],$_SESSION['login']['maid'],$_POST['idrequest'],$_POST['nameproject'],$_POST['price'],$_POST['description'],$_POST['timestart'],$_POST['timedoing'],$_POST['link'],$_POST['insurance'],$_POST['status']);
    }
    public function removerprojectAction()
    {
        echo SoftWares::removeduan($_POST['idduan']);
    }
    public function finishprojectAction()
    {
        echo SoftWares::finishduan($_POST['idduan']);
    }
    public function sendrqorderAction()
    {
        echo SoftWares::sendrequest($_POST['description'],$_POST['note'],$_SESSION['login']['name']);
    }
    public function getdescriptionsAction()
    {
        echo SoftWares::getdescriptions($_POST['idrequest']);
    }
    
}
