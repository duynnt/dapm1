<?php 
namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\CartMD;
    class Cart extends \Core\Controller
    {
        public function showcart()
        {           
            $order = CartMD::getallorder(session_id());
            View::render('Clients/Home/cart.php',['orders'=> $order]);
        }
    }
