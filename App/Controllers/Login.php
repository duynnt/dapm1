<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\Products;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Login extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function loginAction()
    {
        View::render('Clients/User/Login.php',['viewshow'=> 'login']);
    }
    public function registerAction()
    {
        View::render('Clients/User/Login.php',['viewshow'=> 'register']);
    }
    public function registeracountAction()
    {
        $status = 0;
        if (isset($_POST['username']) && isset($_POST['password'])){
            $status = User::register($_POST['username'],$_POST['password']);
        }
        echo $status;
    }
    public function checkloginAction()
    {
        $status = -1;
        if (isset($_POST['username']) && isset($_POST['password'])){
            $status = User::checklogin($_POST['username'],$_POST['password']);
        }
        if ($status == 1){
            $getrules = User::getrules($_POST['username']);
            $getidnv = User::getidnv($_POST['username']);
            $idreal = User::getidnreal($_POST['username']);
            $_SESSION['login'] = ["name"=> $_POST['username'],"rules"=>$getrules,"manv"=>$getidnv,"maid"=>$idreal];
            if ($getrules == 0){
                $status = 0;
            }else{
                if ($getrules == 1){
                    $status = 1;
                }else {
                    if ($getrules == 2){
                        $status = 2;
                    }else{
                        if ($getrules == 3){
                            $status = 3;
                        }else{
                            $status = -1;
                            unset($_SESSION['login']);
                        }
                    }
                }
            }
        }else{
            $status = -1;
            unset($_SESSION['login']);
        }
        echo $status;
    }
    
}
