<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\Products;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Category extends \Core\Controller
{
    public function updateCategory() {
        View::render('Clients/Category/updateCategory.php');
    }
}
