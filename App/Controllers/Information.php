<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\Products;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Information extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */

    public function indexAction() {
        View::render('Clients/User/update.php');
    }
}
