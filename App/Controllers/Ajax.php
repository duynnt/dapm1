<?php 
namespace App\Controllers;

use App\Models\CartMD;
use App\Models\Products;
    class Ajax extends \Core\Controller
    {
        public function getproductID(){
            $id = $_POST['id'];            
            echo Products::getProductID($id);            
        }
        public function addtocart(){         
            $idr = $_POST['id'];
            $session = session_id();            
            echo CartMD::addtocart($idr,$session);
        }
        
    }
