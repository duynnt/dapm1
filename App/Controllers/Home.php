<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\Products;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Home extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function indexAction()
    {
        // $products = Products::getAllProduct();     
        // $categorys = Products::getAllCategory();
        if (isset($_SESSION['login'])){
            View::render('Clients/Home/index.php',['viewslide'=> $_SESSION['login']['rules']]);
        }else{
            View::render('Clients/Home/indexlogin.php');
        }
    }

    public function getAllAction() {
        View::render('Clients/Home/index.php');
    }
}
