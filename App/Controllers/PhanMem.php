<?php

namespace App\Controllers;

use \Core\View;
use App\Models\User;
use App\Models\Products;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class PhanMem extends \Core\Controller
{

    /**
     * Show the index page
     *
     * @return void
     */
    public function phanmemAction()
    {
        View::render('Clients/PhanMem/index.php');
    }
    public function buysoftwareAction()
    {
        View::render('Clients/PhanMem/buysoftware.php');
    }
    public function productsAction()
    {
        View::render('Clients/PhanMem/products.php');
    }
    public function addtocartAction()
    {
        View::render('Clients/PhanMem/addtocart.php');
    }
    public function softwareorderAction()
    {
        View::render('Clients/PhanMem/orderSoftware.php');
    }
    public function controlsoftwareAction()
    {
        View::render('Clients/PhanMem/controlsoftware.php');
    }
    public function controlsoftwareassigmentAction()
    {
        View::render('Clients/PhanMem/controlsoftwareassigment.php');
    }
    public function browserequestAction()
    {
        View::render('Clients/PhanMem/browserequest.php');
    }
    public function updateSoftwareAction()
    {
        View::render('Clients/PhanMem/updateSoftware.php');
    }
    public function contentsingleAction()
    {
        if (isset($_SESSION['login'])){
            View::render('Clients/Home/ordersofeware.php');
        }else{
            View::render('Clients/Home/indexlogin.php');
        }
    }
    
}
